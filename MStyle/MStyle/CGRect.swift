//
//  CGRect.swift
//  MStyle
//
//  Created by Minh Luan Tran on 6/14/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import Foundation
import UIKit

func *(left: CGRect, right: CGFloat) -> CGRect {
    return CGRect(origin: left.origin * right, size: left.size * right)
}

func /(left: CGRect, right: CGFloat) -> CGRect {
    return CGRect(origin: left.origin / right, size: left.size / right)
}
