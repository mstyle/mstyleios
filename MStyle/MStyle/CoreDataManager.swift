//
//  CoreDataManager.swift
//  MStyle
//
//  Created by luan on 10/22/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import Photos

class CoreDataManager {
    static var context: NSManagedObjectContext? {
        get {
            let delegate = UIApplication.shared.delegate as? AppDelegate
            
            return delegate?.managedObjectContext
        }
    }
    
    static var photos: [Photo]? {
        get {
            let sortDescriptor = NSSortDescriptor(key: Photo.Key.kLocalId, ascending: false)
            let sortDescriptors = [sortDescriptor]
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Photo.entityName)
            fetchRequest.sortDescriptors = sortDescriptors
            
            do {
                return try(context?.fetch(fetchRequest)) as? [Photo]
            } catch let err {
                print(err)
            }
            return nil
        }
    }
    
    static func createPhotoWithImage(_ image: UIImage, separator1: Int, separator2: Int, type: PhotoType = .full) -> Photo? {
        guard let context = context else {
            return nil
        }
        
        if PHPhotoLibrary.authorizationStatus() != PHAuthorizationStatus.authorized {
            PHPhotoLibrary.requestAuthorization({ (status: PHAuthorizationStatus) -> Void in
            })
        }
        
        if PHPhotoLibrary.authorizationStatus() == PHAuthorizationStatus.authorized {
            CustomPhotoAlbum.sharedInstance.saveImage(image)
        } else {
            PHPhotoLibrary.requestAuthorization({ status in
                CustomPhotoAlbum.sharedInstance.saveImage(image)
            })
        }
        
        return Photo(image: image, separator1: separator1, separator2: separator2, type: type, context: context)
    }
    
    static func createPhotoWithDictionary(_ dictionary: [String: AnyObject]) -> Photo? {
        guard let context = context else {
            return nil
        }
        
        return Photo(dictionary: dictionary, context: context)
    }
    
    // MARK: Save
    static func save() {
        do {
            try context?.save()
        } catch let err {
            print(err)
        }
    }
    
    static func delete(_ id: Int){
        if let photo = getPhotoWithId(id){
            delete(photo)
        }
    }
    
    static func delete(_ photo: Photo) {
        context?.delete(photo)
        save()
    }
    
    // MARK: Local photo
    static func getPhotoWithId(_ id: Int) -> Photo? {
        guard let photos = photos else {
            return nil
        }
        
        for photo in photos where photo.id?.intValue == id {
            return photo
        }
        return nil
    }
}
