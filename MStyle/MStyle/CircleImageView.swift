//
//  CircleImageView.swift
//  MStyle
//
//  Created by Minh Luan Tran on 5/29/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import UIKit

class CircleImageView: UIImageView {

    override var frame: CGRect {
        didSet {
            layer.cornerRadius = frame.width / 2.0
        }
    }

}
