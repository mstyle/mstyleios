//
//  ShareVC.swift
//  MStyle
//
//  Created by luan on 9/25/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit
import Alamofire
import FBSDKShareKit
import FBSDKLoginKit

class ShareVC: UIViewController {
    static let identifier = "ShareVC"
    var minWidthAddCell: CGFloat = 76
    
    // MARK: Outlet
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tagCollectionView: UICollectionView!
    @IBOutlet weak var tagCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var mstyleButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    
    // MARK: Variables
    var hasPost = false
    var photo: Photo!
    var image: UIImage?
    
    lazy var tempTagNameLabel: UILabel! = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightMedium)
        return label
    }()
    var tagsName = [String]()
    var addingNewTag = false
    
    var heightKeyboard: CGFloat = 0.0
    
    // MARK: Override View Controller
    override func viewDidLoad() {
        super.viewDidLoad()

        imageView.image = image
        tagCollectionView.layer.borderWidth = 1
        tagCollectionView.layer.borderColor = UIColor(red: 168.0/255, green: 168.0/255, blue: 168.0/255, alpha: 1.0).cgColor
        tagCollectionView.layer.cornerRadius = 20
        if let layout = tagCollectionView.collectionViewLayout as? TagShareLayout {
            layout.delegate = self
        }
        navigationItem.setHidesBackButton(true, animated: false)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    // MARK: Action
    @IBAction func addTag() {
        let cell: UICollectionViewCell?
        let addTagIndexPath = IndexPath(row: tagsName.count, section: 0)
        
        if !addingNewTag {
            addingNewTag = true
            tagCollectionView.insertItems(at: [addTagIndexPath])
        }
        cell = tagCollectionView.cellForItem(at: addTagIndexPath)
        
        if let addCell = cell as? AddTagCell {
            addCell.nameTextField.becomeFirstResponder()
        }
    }
    
    @IBAction func toggleFacebook(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func toggleMStyle(_ sender: UIButton) {
        //sender.selected = !sender.selected
    }
    
    @IBAction func share() {
        if mstyleButton.isSelected && !hasPost {
            if let photoId = photo.id {
                showHUDUploading()
                MStyleServer.createPost(contentTextView.text, photoId: Int(photoId), completion: { [weak self] response in
                    guard let sSelf = self else {
                        return
                    }
                    
                    if case Result.success( _) = response.result {
                        showHUDUploaded()
                        sSelf.hasPost = true
                        
                        if sSelf.facebookButton.isSelected {
                            sSelf.startShareFacebook()
                        } else {
                            sSelf.goToHome()
                        }
                    } else {
                        showHUDError()
                    }
                })
            } else {
                photo.discussion = contentTextView.text
                CoreDataManager.save()
            }
        } else if facebookButton.isSelected {
            startShareFacebook()
        }
    }
    
    @IBAction func goToHome() {
        navigationController?.popToRootViewController(animated: true)
    }
}

// MARK: Share Facebook
extension ShareVC: FBSDKSharingDelegate {
    fileprivate func startShareFacebook() {
        shareFacebook(image, text: contentTextView.text, fromViewController: self, delegate: self)
    }
    
    fileprivate func shareFBByGraphRequest() {
        let parameters = ["name": "Sharing Tutorial",
                          "caption": "Build great social apps and get more installs.",
                          "description": "Allow your users to share stories on Facebook from your app using the iOS SDK.",
                          "link": "https://developers.facebook.com/docs/ios/share/",
                          "picture": "http://i.imgur.com/g3Qc1HN.png"]
        
        
        
        let request = FBSDKGraphRequest(graphPath: "/me/feed", parameters: parameters, httpMethod: "POST")
        _ = request?.start(completionHandler: { (_, _, _) in
        })
    }
    
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable: Any]!) {
        print("didCompleteWithResults \(results)")
        goToHome()
    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        print("didFailWithError \(error)")
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        print("sharerDidCancel")
    }
}

// MARK: Tags Collection View DataSource
extension ShareVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return addingNewTag ? tagsName.count + 1 : tagsName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if addingNewTag && indexPath.row == tagsName.count {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AddTagCell.kIdentifier, for: indexPath) as! AddTagCell
            cell.nameTextField.delegate = self
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TagCell.kIdentifier, for: indexPath) as! TagCell
            cell.updateUI(tagsName[indexPath.row])
            cell.deleteButton.tag = indexPath.row
            cell.delegate = self
            
            return cell
        }
    }
}

// MARK: Tas Collection View Delegate
extension ShareVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if addingNewTag && indexPath.row >= tagsName.count {
            
            return
        }
        
        tagsName.remove(at: indexPath.row)
        tagCollectionView.deleteItems(at: [indexPath])
    }
}

// MARK: Textfield Delegate
extension ShareVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        addingNewTag = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if !addingNewTag {
            return false
        }
        
        if string == " " {
            _ = addTagFromTextField(textField)
            return false
        }
        
        let oldString = textField.text! as NSString
        let newString = oldString.replacingCharacters(in: range, with: string)
        textField.text = newString
        
        let expectSize = textField.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude))
        let margin: CGFloat = 8
        let insetMargin: CGFloat = 4
        let newWidth = expectSize.width + margin * 2 + insetMargin
        
        if newWidth >= minWidthAddCell {
            guard let cell = tagCollectionView.cellForItem(at: IndexPath(row: tagsName.count, section: 0)) else {
                return false
            }
            
            cell.frame.size = CGSize(width: newWidth, height: cell.frame.height)
        }
        
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return addTagFromTextField(textField)
    }
    
    fileprivate func addTagFromTextField(_ textField: UITextField) -> Bool {
        guard let tag = textField.text else {
            return false
        }
        if tag.isEmpty {
            return false
        }
        
        textField.text = ""
        
        let insertIndexPath = IndexPath(row: tagsName.count, section: 0)
        tagsName.append(tag)
        
        tagCollectionView.performBatchUpdates({
            self.tagCollectionView.insertItems(at: [insertIndexPath])
        }) { finished in
            self.tagScrollToBottom()
        }
        
        return true
    }
    
    fileprivate func tagScrollToBottom() {
        let bottomOffset = CGPoint(x: 0, y: max(tagCollectionView.contentSize.height - tagCollectionView.bounds.height, 0))
        tagCollectionView.setContentOffset(bottomOffset, animated: true)
    }
}


// MARK: TagCellDelegate
extension ShareVC: TagCellDelegate {
    func deleteCell(_ cell: UICollectionViewCell) {
        if let indexPath = tagCollectionView.indexPath(for: cell) {
            tagsName.remove(at: indexPath.row)
            tagCollectionView.deleteItems(at: [indexPath])
        }
    }
}

// MARK: Collection Layout
extension ShareVC: TagShareLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, sizeTagAtIndexPath indexPath:IndexPath) -> CGSize {
        if addingNewTag && indexPath.row == tagsName.count {
            
            return CGSize(width: minWidthAddCell, height: 22)
        } else {
            let buttonWidth: CGFloat = 16
            let distance: CGFloat = 8
            
            tempTagNameLabel.text = tagsName[indexPath.row]
            tempTagNameLabel.sizeToFit()
            var widthCell = tempTagNameLabel.bounds.size.width
            widthCell += buttonWidth + distance * 3
            
            return CGSize(width: widthCell, height: 22)
        }
    }
    
    func sizeForAddButton(_ collectionView: UICollectionView) -> CGSize {
        return addButton.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, newHeight height: CGFloat) {
        tagCollectionHeight.constant = max(height, 42)
    }
}

// MARK: Keyboard
extension ShareVC {
    func keyboardWillShow(_ notification: Notification) {
        print("Frame Y offset: \(self.view.frame.origin.y)")
        if self.view.frame.origin.y == 0, let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if contentTextView.isFirstResponder {
                let rect = view.convert(contentTextView.frame, from: contentTextView.superview)
                let maxY = rect.maxY
                heightKeyboard = keyboardSize.height - (view.frame.size.height - maxY)
            } else {
                heightKeyboard = keyboardSize.height
            }
            view.frame.origin.y -= max(heightKeyboard, 0)
        }
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if self.view.frame.origin.y < 0 {
            
            self.view.frame.origin.y += heightKeyboard
        }
    }
    
    func dismissKeyboard() {
        if addingNewTag {
            view.endEditing(true)
            if tagsName.count > 0 {
                let removeIndexPath = IndexPath(row: tagsName.count, section: 0)
                addingNewTag = false
                self.tagCollectionView.deleteItems(at: [removeIndexPath])
            }
        } else if (contentTextView.isFirstResponder) {
            contentTextView.resignFirstResponder()
        }
    }
}

extension ShareVC: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        
    }
}
