//
//  ClosetVC.swift
//  MStyle
//
//  Created by luan on 5/22/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import UIKit

class ClosetVC: UIViewController {
    
    fileprivate let photoDetailSegue = "photoDetailSegue"
    var user: User!
    fileprivate var mixedPhotos: [Photo] = []
    fileprivate var lovePhotos: [Photo] = []
    fileprivate var photos: [Photo] = []
    
    fileprivate enum FilterMode {
        case all
        case love
        case mixed
    }
    fileprivate var filterMode = FilterMode.all {
        willSet {
            switch filterMode {
            case .all:
                allPhotoButton.isChoice = false
            case .love:
                lovePhotoButton.isChoice = false
            case .mixed:
                mixPhotoButton.isChoice = false
            }
        }
        didSet {
            photosCollectionView.collectionViewLayout = closetCollectionLayout
            switch filterMode {
            case .all:
                photos = user.photos
                allPhotoButton.isChoice = true
            case .love:
                photos = lovePhotos
                lovePhotoButton.isChoice = true
            case .mixed:
                photos = mixedPhotos
                mixPhotoButton.isChoice = true
            }
            closetCollectionLayout.didLoad = false
            photosCollectionView.performBatchUpdates({
                self.photosCollectionView.reloadSections(IndexSet(integer: 0))
            }, completion: nil)
        }
    }
    // MARK: Outlet
    @IBOutlet weak var avatarImageView: UIImageView!
    
    @IBOutlet weak var photoLabel: UILabel!
    @IBOutlet weak var mixLabel: UILabel!
    @IBOutlet weak var friendLabel: UILabel!
    
    @IBOutlet weak var photosCollectionView: UICollectionView!
    
    var closetCollectionLayout: ClosetCollectionLayout!
    var flowLayout = UICollectionViewFlowLayout()
    
    @IBOutlet weak var allPhotoButton: ClosetFilterButton!
    @IBOutlet weak var lovePhotoButton: ClosetFilterButton!
    @IBOutlet weak var mixPhotoButton: ClosetFilterButton!
    @IBOutlet weak var deleteView: UIView!
    
    @IBOutlet var backBarButtonItem: UIBarButtonItem!
    
    var deleteIndexs = [Int]()
    var deleteMode = false {
        didSet {
            photosCollectionView.allowsMultipleSelection = deleteMode
            deleteView.isHidden = !deleteMode
            
        }
    }
    
    // MARK: Override ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
    
        closetCollectionLayout = photosCollectionView.collectionViewLayout as! ClosetCollectionLayout
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        user.delegate = self
        updateUserUI()
        updatePhotosUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if user == nil {
            user = User.currentUser
        }
        if user === User.currentUser {
            navigationItem.leftBarButtonItem = nil
        } else {
            navigationItem.leftBarButtonItem = backBarButtonItem
            navigationItem.rightBarButtonItem = nil
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == photoDetailSegue {
            if let photo = sender as? Photo, let vc = segue.destination as? PhotoVC {
                vc.photo = photo
                vc.user = user
            }
        }
    }
    
    // MARK: Action
    @IBAction func back() {
        if navigationController?.popViewController(animated: true) == nil {
            tabBarController?.selectedIndex = 0
        }
    }
    
    @IBAction func startDeletePhoto() {
        deleteMode = true
    }
    
    @IBAction func cancelDeletePhoto() {
        deleteMode = false
        var indexPaths = [IndexPath]()
        while deleteIndexs.count > 0 {
            guard let index = deleteIndexs.popLast() else {
                continue
            }
            indexPaths.append(IndexPath(item: index, section: 0))
        }
        photosCollectionView.reloadItems(at: indexPaths)
    }
    
    func performDelete() {
        var deletePhotos = [Photo]()
        for index in deleteIndexs {
            guard index >= 0 && index < photos.count else {
                continue
            }
            deletePhotos.append(photos[index])
        }
        showHUDLoading()
        MStyleServer.deletePhotos(deletePhotos) { [weak self] success in
            if success {
                showHUDSuccess()
                for photo in deletePhotos {
                    CoreDataManager.delete(photo)
                }
                User.currentUser.photos = CoreDataManager.photos ?? []
                self?.deleteIndexs = []
                self?.updatePhotosUI()
            } else {
                showHUDError()
            }
        }
    }
    
    @IBAction func deletePhoto() {
        guard deleteMode else {
            return
        }
        guard deleteIndexs.count > 0 else {
            return
        }
        let alertVC = UIAlertController(title: "Delete photos", message: "Are you sure?", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { _ in
            self.performDelete()
        }
        alertVC.addAction(cancelAction)
        alertVC.addAction(deleteAction)
        
        present(alertVC, animated: true, completion: nil)
    }
    
    @IBAction func filterMain() {
        guard filterMode != .all else {
            return
        }
        filterMode = .all
    }
    
    @IBAction func filterLove() {
        guard filterMode != .love else {
            return
        }
        filterMode = .love
    }
    
    @IBAction func filterMix() {
        guard filterMode != .mixed else {
            return
        }
        filterMode = .mixed
    }
    
    // MARK: Bind User Data
    fileprivate func updateUserUI() {
        navigationItem.title = user.fullName
        if let url = user.avatarUrl {
            avatarImageView.hnk_setImageFromURL(url)
        }
    }
    
    fileprivate func updatePhotosUI() {
        photos = user.photos
        mixedPhotos = user.photos.filter { photo -> Bool in
            return photo.type == .mixed
        }
        /*lovePhotos = user.photos.sorted(by: { left, right -> Bool in
            return left.createdDate.compare(right.createdDate) == .orderedAscending
        })*/
        lovePhotos = user.photos.filter { photo -> Bool in
            return photo.favorited
        }
        photoLabel.text = String(photos.count)
        mixLabel.text = String(mixedPhotos.count)
        closetCollectionLayout.didLoad = false
        photosCollectionView.performBatchUpdates({
            self.photosCollectionView.reloadSections(IndexSet(integer: 0))
        }, completion: nil)
    }
}

// MARK: CollectionViewDataSource
extension ClosetVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClosetCell", for: indexPath) as! ClosetCell
        cell.showWith(photo: photos[indexPath.item])
        cell.isChooseDelete = deleteIndexs.contains(indexPath.row)
        return cell
    }
}
extension ClosetVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let margin: CGFloat = 4.0
        let width = photosCollectionView.bounds.width - margin * 2
        let height: CGFloat
        let photo = photos[indexPath.row]
        if let image = photo.image {
            height = width * image.size.height / image.size.width + margin * 2
        } else {
            height = photosCollectionView.bounds.height - 24
        }
        return CGSize(width: width, height: height)
    }
}

// MARK: CollectionViewDelegate
extension ClosetVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if deleteMode {
            let index = indexPath.item
            if deleteIndexs.contains(index) {
                deleteIndexs.removeFirst(index)
            } else {
                deleteIndexs.append(index)
            }
            collectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
            return
        }
        performSegue(withIdentifier: photoDetailSegue, sender: photos[indexPath.item])
    }
}

// MARK: User Delegate
extension ClosetVC: UserDelegate {
    func didUpdateInfo(user: User) {
        updateUserUI()
    }

    func didGetAllPhoto(user: User) {
        updatePhotosUI()
    }
    
    func didDownload(photo: Photo, user: User) {
        guard self.user === user else {
            return
        }
        guard let index = photos.index(of: photo) else {
            return
        }
        guard let cell = photosCollectionView.cellForItem(at: IndexPath(item: index, section: 1)) as? ClosetCell else {
            return
        }
        cell.showWith(photo: photo)
    }
    
    func didAddPhoto(photo: Photo, user: User) {
        
    }
}
