//
//  Function.swift
//  MStyle
//
//  Created by luan on 8/23/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import Foundation
import UIKit
import PKHUD

let applicationDocumentsDirectory: String = {
    let paths = NSSearchPathForDirectoriesInDomains(
        .documentDirectory, .userDomainMask, true)
    return paths[0]
}()

let applicationCachesDirectory: String = {
    let paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
    return paths[0]
}()

func viewController(withIdentifier identifier: String) -> UIViewController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier)
}

var showFirstPage = true

// MARK: Time
let secondPerMinute = 60
let minutePerHour = 60
let hourPerDay = 24


var dateFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    return dateFormatter
}()

func dateFromString(_ string: String) -> Date? {
    return dateFormatter.date(from: string)
}

func dentaStringFromDate(_ date: Date) ->String {
    let curDate = Date()
    let denta = curDate.timeIntervalSince(date)
    
    let seconds = lround(denta)
    
    if seconds < secondPerMinute {
        return "Just now"
    }
    
    let minute = seconds / secondPerMinute
    if minute < minutePerHour {
        return "\(minute) min\(minute > 1 ? "s" : "")"
    }
    
    let hour = minute / minutePerHour
    if hour < hourPerDay {
        return "\(hour) hour\(hour > 1 ? "s" : "")"
    }
    
    let days = hour / hourPerDay
    let dateFormatter = DateFormatter()
    
    if days == 1 {
        dateFormatter.dateFormat = "'Yesterday at 'h:mma"
        return dateFormatter.string(from: date)
    }
    
    let curComponents = (Calendar.current as NSCalendar).components([.month, .year], from: curDate)
    let components = (Calendar.current as NSCalendar).components([.month, .year], from: date)
    if components.year == curComponents.year && components.month == curComponents.month {
        dateFormatter.dateFormat = "MMM d at h:mma"
    } else if components.year == curComponents.year {
        dateFormatter.dateFormat = "MMM d"
    } else {
        dateFormatter.dateFormat = "MMM d, yyyy"
    }
    
    return dateFormatter.string(from: date)
}

// MARK: Math

func rad(_ degrees: CGFloat) -> CGFloat {
    return degrees * CGFloat(Float.pi) / 180.0
}

// MARK: HUD

func hideHUD() {
    HUD.hide(animated: true)
}

func hideHUD(_ delay: TimeInterval) {
    HUD.hide(afterDelay: delay)
}

func showHUDDownloading() {
    HUD.flash(.labeledError(title: "Downloading", subtitle: "Please wait the progress finish"), delay: 2.0)
}

func showHUDLoading() {
    HUD.show(.progress)
}

func showHUDUploading() {
    HUD.show(.labeledProgress(title: "Uploading", subtitle: nil))
}

func showHUDUploaded() {
    HUD.flash(.labeledSuccess(title: "Uploaded", subtitle: nil), delay: 0.6)
}

func showHUDError() {
    HUD.flash(.labeledError(title: "Error", subtitle: "Please try again"), delay: 2.0)
}

func showHUDDeleted() {
    HUD.flash(.labeledSuccess(title: "Deleted", subtitle: nil), delay: 0.6)
}

func showHUDFail() {
    HUD.flash(.error, delay: 0.6)
}

func showHUDSuccess(completion: ((Bool) -> Void)? = nil) {
    HUD.flash(.success, onView: nil, delay: 0.6, completion: completion)
}
