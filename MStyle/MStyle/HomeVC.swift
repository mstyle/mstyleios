//
//  HomeVC.swift
//  MStyle
//
//  Created by luan on 10/7/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {
    
    enum DataSourceType {
        case appTitle
        case friendsRequest
        case friendsSuggest
        case friends
    }
    
    fileprivate let showPhotoSegue = "photoDetailSegue"
    fileprivate let goFriendClosetSegue = "goFriendCloset"
    
    // MARK: Outlet
    @IBOutlet weak var userTableView: UITableView!
    @IBOutlet var headerView: UIView!
    @IBOutlet var friendRequestHeaderView: UIView!
    @IBOutlet var friendSuggestHeaderView: UIView!
    
    // MARK: Variables
    var friends: [User] = []
    var suggestingFriends: [User] = []
    var requestFriends: [User] = []
    var hideSuggestion: Bool = false
    var hideRequestion: Bool = false
    
    // MARK: Override ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MStyleServer.getFriends { [weak self] users in
            guard let sSelf = self else {
                return
            }
            sSelf.friends = users
            if users.count > 0 {
                let section = sSelf.section(atDataSourceType: .friends)
                
                sSelf.userTableView.beginUpdates()
                sSelf.userTableView.insertSections(IndexSet(integer: section), with: .automatic)
                sSelf.userTableView.endUpdates()
                
                for user in users {
                    user.getAllPhoto()
                }
            }
        }
        MStyleServer.getFriendsSuggesting { [weak self] users in
            guard let sSelf = self else {
                return
            }
            sSelf.suggestingFriends = users
            if users.count > 0 {
                let section = sSelf.section(atDataSourceType: .friendsSuggest)
                
                sSelf.userTableView.beginUpdates()
                sSelf.userTableView.insertSections(IndexSet(integer: section), with: .automatic)
                sSelf.userTableView.endUpdates()
            }
        }
        MStyleServer.getFriendsRequest { [weak self] users in
            guard let sSelf = self else {
                return
            }
            
            sSelf.requestFriends = users
            if users.count > 0 {
                let section = sSelf.section(atDataSourceType: .friendsRequest)
                
                sSelf.userTableView.beginUpdates()
                sSelf.userTableView.insertSections(IndexSet(integer: section), with: .automatic)
                sSelf.userTableView.endUpdates()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    
        navigationController?.isNavigationBarHidden = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == showPhotoSegue {
            if let photoDetailVC = segue.destination as? PhotoVC,
                let photo = sender as? Photo {
                
                photoDetailVC.user = photo.owner
                photoDetailVC.photo = photo
            }
        } else if segue.identifier == goFriendClosetSegue {
            if let closetVC = segue.destination as? ClosetVC,
                let friend = sender as? User {
                closetVC.user = friend
            }
        }
    }
    
    // MARK: Action
    @IBAction func hideSuggestion(sender: UIButton) {
        hideSuggestion = !hideSuggestion
        sender.setTitle(hideSuggestion ? "show" : "hide", for: .normal)
        
        let suggestSection = section(atDataSourceType: .friendsSuggest)
        var indexPaths = [IndexPath]()
        for i in 0..<suggestingFriends.count {
            indexPaths.append(IndexPath(row: i, section: suggestSection))
        }
        
        userTableView.beginUpdates()
        if hideSuggestion {
            userTableView.deleteRows(at: indexPaths, with: .left)
        } else {
            userTableView.insertRows(at: indexPaths, with: .right)
        }
        userTableView.endUpdates()
    }
    
    @IBAction func hideRequestion(sender: UIButton) {
        hideRequestion = !hideRequestion
        sender.setTitle(hideRequestion ? "show" : "hide", for: .normal)
        
        let requestSection = section(atDataSourceType: .friendsRequest)
        var indexPaths = [IndexPath]()
        for i in 0..<requestFriends.count {
            indexPaths.append(IndexPath(row: i, section: requestSection))
        }
        
        userTableView.beginUpdates()
        if hideRequestion {
            userTableView.deleteRows(at: indexPaths, with: .left)
        } else {
            userTableView.insertRows(at: indexPaths, with: .right)
        }
        userTableView.endUpdates()
        
    }
}

// MARK: TableView Datasource
extension HomeVC: UITableViewDataSource {
    fileprivate func section(atDataSourceType type: DataSourceType) -> Int {
        switch type {
        case .appTitle:
            return 0
        case .friendsRequest:
            return 1
        case .friendsSuggest:
            return requestFriends.count > 0 ? 2 : 1
        case .friends:
            var section = 1
            if requestFriends.count > 0 {
                section += 1
            }
            if suggestingFriends.count > 0 {
                section += 1
            }
            return section
        }
    }
    
    fileprivate func dataSourceType(atSection section: Int) -> DataSourceType {
        switch section {
        case 0:
            return .appTitle
        case 1:
            if requestFriends.count > 0 {
                return .friendsRequest
            } else if suggestingFriends.count > 0 {
                return .friendsSuggest
            } else {
                return .friends
            }
        case 2:
            if requestFriends.count > 0 && suggestingFriends.count > 0 {
                return .friendsSuggest
            } else {
                return .friends
            }
        default:
            return .friends
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var sections = 1
        if friends.count > 0 {
            sections += 1
        }
        if suggestingFriends.count > 0 {
            sections += 1
        }
        if requestFriends.count > 0 {
            sections += 1
        }
        return sections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch dataSourceType(atSection: section) {
        case .appTitle:
            return 0
        case .friendsRequest:
            return hideRequestion ? 0 : requestFriends.count
        case .friendsSuggest:
            return hideSuggestion ? 0 : suggestingFriends.count
        case .friends:
            return friends.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch dataSourceType(atSection: indexPath.section) {
        case .appTitle:
            return UITableViewCell()
        case .friendsRequest:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FriendRequestCell", for: indexPath) as! FriendRequestCell
            cell.friend = requestFriends[indexPath.row]
            cell.last = indexPath.row == requestFriends.count - 1
            cell.acceptFriendHandle = {
                [weak self] friendId in
                
                guard let sSelf = self else {
                    return
                }
                let section = sSelf.section(atDataSourceType: .friendsRequest)
                for (index, friend) in sSelf.requestFriends.enumerated() {
                    if friend.id == friendId {
                        sSelf.requestFriends.remove(at: index)
                        
                        sSelf.userTableView.beginUpdates()
                        if sSelf.requestFriends.count == 0 {
                            sSelf.userTableView.deleteSections(IndexSet(integer: section), with: .left)
                        } else {
                            sSelf.userTableView.deleteRows(at: [IndexPath(row: index, section: section)], with: .left)
                        }
                        
                        friend.getAllPhoto()
                        let friendSection = sSelf.section(atDataSourceType: .friends)
                        sSelf.friends.append(friend)
                        if sSelf.friends.count == 1 {
                            sSelf.userTableView.insertSections(IndexSet(integer: friendSection), with: .right)
                        } else {
                            sSelf.userTableView.insertRows(at: [IndexPath(row: sSelf.friends.count, section: friendSection)], with: .right)
                        }
                        sSelf.userTableView.endUpdates()
                        break
                    }
                }
                MStyleServer.acceptFriend(friendId)
            }
            cell.deniedFriendHandle = {
                [weak self] friendId in
                
                guard let sSelf = self else {
                    return
                }
                let section = sSelf.section(atDataSourceType: .friendsRequest)
                for (index, friend) in sSelf.requestFriends.enumerated() {
                    if friend.id == friendId {
                        sSelf.requestFriends.remove(at: index)
                        
                        sSelf.userTableView.beginUpdates()
                        if sSelf.requestFriends.count == 0 {
                            sSelf.userTableView.deleteSections(IndexSet(integer: section), with: .left)
                        } else {
                            sSelf.userTableView.deleteRows(at: [IndexPath(row: index, section: section)], with: .left)
                        }
                        sSelf.userTableView.endUpdates()
                        break
                    }
                }
                MStyleServer.denyFriend(friendId)
            }
            
            return cell
        case .friendsSuggest:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FriendSuggestingCell", for: indexPath) as! FriendSuggestCell
            cell.friend = suggestingFriends[indexPath.row]
            cell.last = indexPath.row == suggestingFriends.count - 1
            cell.addFriendHandle = {
                [weak self] friendID in
                
                guard let sSelf = self else {
                    return
                }
                let section = sSelf.section(atDataSourceType: .friendsSuggest)
                var index = 0
                for friend in sSelf.suggestingFriends {
                    if friend.id == friendID {
                        sSelf.suggestingFriends.remove(at: index)
                        sSelf.userTableView.beginUpdates()
                        if sSelf.suggestingFriends.count > 0 {
                            sSelf.userTableView.deleteRows(at: [
                                IndexPath(row: index, section: section)], with: .left)
                        } else {
                            sSelf.userTableView.deleteSections(IndexSet(integer: section), with: .left)
                        }
                        sSelf.userTableView.endUpdates()
                        break
                    }
                    index += 1
                }
                MStyleServer.requestFriend(friendID)
            }
            
            return cell
        case .friends:
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! FriendCell
            cell.friend = friends[indexPath.row]
            cell.didPressImage = { [weak self] photo in
                guard self != nil else {
                    return
                }
                
                self!.performSegue(withIdentifier: self!.showPhotoSegue, sender: photo)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch dataSourceType(atSection: indexPath.section) {
        case .appTitle:
            return 0
        case .friendsRequest:
            return 61
        case .friendsSuggest:
            return 66
        case .friends:
            return 80 + view.bounds.width / 2 + 48
        }
    }
}

// MARK: TableView Delegate
extension HomeVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch dataSourceType(atSection: section) {
        case .appTitle:
            return headerView
        case .friendsRequest:
            return friendRequestHeaderView
        case .friendsSuggest:
            return friendSuggestHeaderView
        case .friends:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch dataSourceType(atSection: section) {
        case .appTitle:
            return 44
        case .friendsRequest:
            return 38
        case .friendsSuggest:
            return 38
        case .friends:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard dataSourceType(atSection: indexPath.section) == .friends else {
            return
        }
        
        performSegue(withIdentifier: goFriendClosetSegue, sender: friends[indexPath.item])    }
}
