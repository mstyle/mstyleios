//
//  SlideOutAnimationController.swift
//  MStyle
//
//  Created by luan on 8/29/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit

class SlideOutAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        if let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from) {
            let containerView = transitionContext.containerView
            
            let duration = transitionDuration(using: transitionContext) as TimeInterval
            UIView.animate(withDuration: duration, animations: {
                fromView.center.y -= containerView.bounds.height
                fromView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            }, completion: { finished in
                    transitionContext.completeTransition(finished)
            })
        }
    }
}
