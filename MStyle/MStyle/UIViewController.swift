//
//  UIViewController.swift
//  MStyle
//
//  Created by luan on 10/28/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit

extension UIViewController {
    var isVisible: Bool {
        get {
            return isViewLoaded && self.view.window != nil
        }
    }
}
