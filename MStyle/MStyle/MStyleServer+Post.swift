//
//  MStyleServer+Post.swift
//  MStyle
//
//  Created by luan on 10/1/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import Foundation
import Alamofire

extension MStyleServer {
    static func createPost(_ discussion: String, photoId: Int, completion: ((DataResponse<Any>) -> Void)? = nil) {
        let URL = HOST + POST_END_POINT
        let params: [String: AnyObject] = [
            "discussion": discussion as AnyObject,
            "photoId": photoId as AnyObject
        ]
        
        let request = Alamofire.request(URL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers)
        request.validate().responseJSON { (response) in
            completion?(response)
        }
    }
    
    static func createPost(_ post: Post, completion: ((DataResponse<Any>) -> Void)? = nil) {
        let URL = HOST + POST_END_POINT
        
        let request = Alamofire.request(URL, method: .post, parameters: post.dictionary, encoding: JSONEncoding.default, headers: headers)
        request.validate().responseJSON { response in
            completion?(response)
        }
    }
    
    static func getPosts(_ page: Int, size: Int, completion: (([Post]) -> Void)? = nil) {
        let url = HOST + WALL_END_POINT + "?sort=createdDate,desc&page=\(page)&size=\(size)"
        
        let request = Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
        request.validate().responseJSON { response in
            
            var posts = [Post]()
            if case .success(let value) = response.result, let postDictionaries = value as? [[String: AnyObject]] {
                for postDictionary in postDictionaries {
                    let post = Post(dictionary: postDictionary)
                    posts.append(post)
                }
            }
            completion?(posts)
        }
    }
    
    static func likePost(_ post: Post, completion: ((_ success: Bool, _ id: Int) -> Void)? = nil) {
        guard let id = post.id else {
            completion?(false, 0)
            return
        }
        
        let url = HOST + POST_LIKE_POINT
        let parameters = [
            "postId": id,
            "userId": UserDefaultsHelper.integerForKey(UserDefaultsKey.userId)
        ]
        let request = Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        request.validate().responseJSON { response in
            completion?(response.result.isSuccess, id)
        }
    }
}
