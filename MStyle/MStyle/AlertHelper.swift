//
//  Utils.swift
//  SMEPortal
//
//  Created by Phu Nguyen Quang on 10/15/15.
//  Copyright © 2015 TRAN KHANH TUNG. All rights reserved.
//

import Foundation
import UIKit

struct AlertHelper {
    
    static func showAlertWithTitle(_ title:String, message:String, target: UIViewController, completion:(() -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { alertAction in
            completion?()
        }))
        target.present(alert, animated: true, completion: nil)
    }
    
    /*static func showToastMessage(message: String) {
        let window = UIApplication.sharedApplication().keyWindow
        window?.makeToast(message, duration: 1, position: CSToastPositionBottom)
    }*/

}

