//
//  ClosetPopupPresentation.swift
//  MStyle
//
//  Created by luan on 8/28/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit

class ClosetPopupPresentation: UIPresentationController {

    fileprivate lazy var dimmingView: UIView = {
        let view = UIView(frame: CGRect.zero)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        return view
    }()
    
    override var shouldRemovePresentersView : Bool {
        return false
    }
    
    override func presentationTransitionWillBegin() {
        super.presentationTransitionWillBegin()
        
        dimmingView.frame = containerView!.bounds
        containerView!.insertSubview(dimmingView, at: 0)
        
        dimmingView.alpha = 0
        if let transitionCoordinator = presentedViewController.transitionCoordinator {
            transitionCoordinator.animate(alongsideTransition: { _ in
                self.dimmingView.alpha = 1
                }, completion: nil)
        }
    }
    
    override func dismissalTransitionWillBegin() {
        super.dismissalTransitionWillBegin()
        
        if let transitionCoordinator = presentedViewController.transitionCoordinator {
            transitionCoordinator.animate(alongsideTransition: { _ in
                self.dimmingView.alpha = 0
                }, completion: nil)
        }
    }
}
