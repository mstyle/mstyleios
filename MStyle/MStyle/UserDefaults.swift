//
//  UserDefaults.swift
//  MStyle
//
//  Created by luan on 8/24/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import Foundation

enum UserDefaultsKey {
    case firstRun
    case localPhotoId
    case userId
    case styleID
    
    var string: String {
        switch self {
        case .userId: return "user_id"
        case .firstRun: return "FirstRun"
        case .localPhotoId: return "LocalPhotoId"
        case .styleID: return "StyleID"
        }
    }
}

struct UserDefaultsHelper {
    static func isExist(_ key: UserDefaultsKey) -> Bool {
        return UserDefaults.standard.object(forKey: key.string) != nil
    }
    
    static func setValue(_ value: AnyObject, key: UserDefaultsKey) {
        UserDefaults.standard.setValue(value, forKey: key.string)
    }
    
    static func setInteger(_ value: Int, forKey: UserDefaultsKey) {
        UserDefaults.standard.set(value, forKey: forKey.string)
    }
    
    static func integerForKey(_ key: UserDefaultsKey) -> Int {
        return UserDefaults.standard.integer(forKey: key.string)
    }
    
    static func synchronize() {
        UserDefaults.standard.synchronize()
    }
}
