//
//  UIImageView.swift
//  MStyle
//
//  Created by luan on 8/30/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit

extension UIImageView {
    var frameForImage: CGRect {
        guard let image = image else {
            return CGRect.zero
        }
        
        switch contentMode {
        case .scaleAspectFit:
            let imageRatio = image.size.width / image.size.height
            let imageViewRatio = bounds.width / bounds.height
            if imageRatio < imageViewRatio {
                let width = bounds.height * imageRatio
                return CGRect(x: (bounds.width - width) / 2, y: 0, width: width, height: bounds.height)
            } else {
                let height = bounds.width / imageRatio
                return CGRect(x: 0, y: (bounds.height - height) / 2, width: bounds.width, height: height)
            }
        default:
            return bounds
        }
    }
}
