//
//  TagsPartsVC.swift
//  MStyle
//
//  Created by Phu Quang Nguyen on 5/26/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import CoreImage

class TagsPartsVC: UIViewController {

    let segueClosetIdentifier = "ShowCloset"
    // MARK: Outlet
    @IBOutlet weak var capturedImageView: UIImageView!
    
    @IBOutlet weak var modeSwitch: UISwitch!
    @IBOutlet weak var friendLabel: UILabel!
    @IBOutlet weak var privateLabel: UILabel!
    @IBOutlet weak var contraistSlider: UISlider!
    
    fileprivate var isPrivate: Bool = false {
        didSet {
            if isPrivate {
                friendLabel.textColor = UIColor(netHex: 0x9B9B9B)
                privateLabel.textColor = UIColor(netHex: 0x4A4A4A)
            } else {
                friendLabel.textColor = UIColor(netHex: 0x4A4A4A)
                privateLabel.textColor = UIColor(netHex: 0x9B9B9B)
            }
        }
    }
    
    // MARK: Variable
    var capturedImage: UIImage!
    var captureCIImage: CIImage?
    var controlFilter: CIFilter?
    var oldValue: Float = 0.0
    
    lazy var tempTagNameLabel: UILabel! = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightMedium)
        return label
    }()
    
    var photo: Photo!
    
    // MARK: View Controller
    override func viewDidLoad() {
        super.viewDidLoad()

        modeSwitch.layer.borderWidth = 1
        modeSwitch.layer.cornerRadius = 16
        modeSwitch.layer.borderColor = UIColor(netHex: 0xE0E0E0).cgColor
        oldValue = contraistSlider.value
        
        capturedImageView.image = capturedImage
        captureCIImage = CIImage(image: capturedImage)
        controlFilter = CIFilter(name: "CIColorControls")
        if let filter = controlFilter,
            let ciImage = captureCIImage {
            filter.setDefaults()
            filter.setValue(ciImage, forKey: kCIInputImageKey)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueClosetIdentifier {
            if let mixVC = segue.destination as? MixVC {
                mixVC.photo = photo
                mixVC.user = User.currentUser
            }
        }
    }
    
    // MARK: Action Trigger
    
    @IBAction func contrastChanged(_ sender: UISlider) {
        let value = sender.value
        if String(format: "%.1f", value) == String(format: "%.1f", oldValue) {
            return
        }
        oldValue = value
        
        DispatchQueue(label: "contrastImage").async { [weak self] in
            guard let filter = self?.controlFilter else {
                return
            }
            filter.setDefaults()
            filter.setValue(value, forKey: kCIInputContrastKey)
            
            guard let outputImage = filter.outputImage else {
                return
            }
            let image = UIImage(ciImage: outputImage)
            DispatchQueue.main.async { [weak self] in
                self?.capturedImageView.image = image
            }
        }
    }
    
    @IBAction func switchPermission(_ sender: UISwitch) {
        isPrivate = sender.isOn
    }
    
    @IBAction func nextTapped(_ sender: AnyObject) {
        if let photo = CoreDataManager.createPhotoWithImage(capturedImage,
                                                            separator1: Int(capturedImage.size.height * 0.333),
                                                            separator2: Int(capturedImage.size.height * 0.75)) {
            self.photo = photo
            User.currentUser.addPhoto(photo)
            CoreDataManager.save()
            photo.uploadServerIfNeed()
        }
        
        performSegue(withIdentifier: segueClosetIdentifier, sender: nil)
    }
    
    @IBAction func cancelTapped() {
        navigationController?.popViewController(animated: true)
    }
    
    
}
