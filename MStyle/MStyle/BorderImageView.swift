//
//  BorderImageView.swift
//  MStyle
//
//  Created by luan on 8/23/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit

@IBDesignable
class BorderImageView: UIImageView {
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
}
