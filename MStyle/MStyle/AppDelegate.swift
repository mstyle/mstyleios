//
//  AppDelegate.swift
//  MStyle
//
//  Created by Phu Quang Nguyen on 5/12/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        if !UserDefaultsHelper.isExist(.firstRun) {
            KeychainHelper.clear()
            
            UserDefaultsHelper.setValue("Anything" as AnyObject, key: .firstRun)
            UserDefaultsHelper.synchronize()
            
            self.window?.rootViewController = viewController(withIdentifier: "WalkThroughVC")
            self.window?.makeKeyAndVisible()
        } else if !MStyleServer.hadAccessToken() && FBSDKAccessToken.current() == nil {
            
            self.window?.rootViewController = viewController(withIdentifier: "LoginViewController")
            self.window?.makeKeyAndVisible()
        }
        
        customAppearance()
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        let handled = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        
        return handled
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // MARK: - Core Data
    lazy var managedObjectContext: NSManagedObjectContext = {
        guard let modelURL = Bundle.main.url(forResource: "Model", withExtension: "momd") else {
            fatalError("Could not find data model in app bundle")
        }
        
        guard let model = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Error initializing model from: \(modelURL)")
        }
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = urls[0]
        let storeURL = documentsDirectory.appendingPathComponent("DataStore.sqlite")
        print("\(storeURL)")
        
        do {
            let coordinator = NSPersistentStoreCoordinator(managedObjectModel: model)
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
            
            let context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
            context.persistentStoreCoordinator = coordinator
            
            return context
        } catch {
            fatalError("Error adding persistent store at \(storeURL): \(error)")
        }
        
    }()

    // MARK: Appearence
    fileprivate func customAppearance() {
        let navfont = UIFont(name: "Roboto", size: 17) ?? UIFont.systemFont(ofSize: 17)
        
        UINavigationBar.appearance().tintColor = UIColor(red: 38.0/255.0, green: 38.0/255.0, blue: 38.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().barTintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [
            NSFontAttributeName: navfont,
            NSForegroundColorAttributeName: UIColor(netHex: 0x4A4A4A)
        ]
        
        UITabBar.appearance().tintColor = UIColor.white
        if let thumbImage = UIImage(named: "slider-line") {
            UISlider.appearance().setThumbImage(thumbImage, for: .normal)
        }
    }
}

