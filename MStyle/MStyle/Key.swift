//
//  Key.swift
//  MStyle
//
//  Created by Phu Quang Nguyen on 5/24/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import Foundation

struct Key {
    static let CaptureButtonPositionX = "CaptureButtonPositionX"
    static let CaptureButtonPositionY = "CaptureButtonPositionY"
}