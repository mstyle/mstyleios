//
//  FriendCell.swift
//  MStyle
//
//  Created by luan on 5/14/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import UIKit

protocol FriendCellDelegate {
    func didTapImageAt(index: Int)
}

class FriendCell: UITableViewCell {

    var friend: User! {
        didSet {
            if let friend = friend {
                if let url = friend.avatarUrl {
                    avatarImageView.hnk_setImageFromURL(url)
                }
                nameLabel.text = friend.fullName
                
                updatePhotosUI()
                friend.delegate = self
            }
        }
    }
    var didPressImage: ((Photo) -> Void)?
    // MARK: Outlet
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var photo1ImageView: UIImageView!
    @IBOutlet weak var photo2ImageView: UIImageView!
    @IBOutlet weak var photo3ImageView: UIImageView!
    @IBOutlet weak var photo4ImageView: UIImageView!
    @IBOutlet weak var photo5ImageView: UIImageView!
    
    @IBOutlet weak var photoLabel: UILabel!
    @IBOutlet weak var mixLabel: UILabel!
    @IBOutlet weak var friendLabel: UILabel!
    
    // MARK: Override TableViewCell
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        photo1ImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapImage(sender:))))
        photo2ImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapImage(sender:))))
        photo3ImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapImage(sender:))))
        photo4ImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapImage(sender:))))
        photo5ImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapImage(sender:))))
    }
    
    override func removeFromSuperview() {
        super.removeFromSuperview()
        
        photo1ImageView.gestureRecognizers?.removeAll()
        photo2ImageView.gestureRecognizers?.removeAll()
        photo3ImageView.gestureRecognizers?.removeAll()
        photo4ImageView.gestureRecognizers?.removeAll()
        photo5ImageView.gestureRecognizers?.removeAll()
    }
    
    func tapImage(sender: UITapGestureRecognizer) {
        if let index = sender.view?.tag, index < friend.photos.count {
            didPressImage?(friend.photos[index])
        }
    }
    
    // MARK: Data
    func updatePhotosUI() {
        let mixeds = friend.photos.filter { photo -> Bool in
            return photo.type == .mixed
        }
        photoLabel.text = String(friend.photos.count)
        mixLabel.text = String(mixeds.count)
        if friend.photos.count > 0 {
            timeLabel.text = dentaStringFromDate(friend.photos.first!.createdDate)
            for i in 0..<5 {
                if i < friend.photos.count, let imageView = imageViewAt(index: i) {
                    imageView.image = friend.photos[i].image
                }
            }
        } else {
            timeLabel.text = ""
        }
    }
    
    func imageViewAt(index: Int) -> UIImageView? {
        switch index {
        case 0:
            return photo1ImageView
        case 1:
            return photo2ImageView
        case 2:
            return photo3ImageView
        case 3:
            return photo4ImageView
        case 4:
            return photo5ImageView
        default:
            return nil
        }
    }
}

// MARK: UserDelegate
extension FriendCell: UserDelegate {
    func didAddPhoto(photo: Photo, user: User) {
    }

    func didDownload(photo: Photo, user: User) {
        guard self.friend === user else {
            return
        }
        if let index = user.photos.index(of: photo), let imageView = imageViewAt(index: index) {
            imageView.image = photo.image
        }
    }

    func didGetAllPhoto(user: User) {
        guard self.friend === user else {
            return
        }
        
        updatePhotosUI()
    }
    
    func didUpdateInfo(user: User) {
    }
}
