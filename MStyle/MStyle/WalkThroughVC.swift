//
//  WalkThroughVC.swift
//  MStyle
//
//  Created by luan on 4/28/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import UIKit

class WalkThroughVC: UIViewController {
    
    // MARK: Outlet
    @IBOutlet weak var okButton: UIButton?
    
    // MARK: View Controller
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let okButton = okButton {
            okButton.layer.borderWidth = 1
            okButton.layer.borderColor = UIColor.white.cgColor
            okButton.layer.cornerRadius = 8
        }
    }
}
