//
//  BorderButton.swift
//  MStyle
//
//  Created by luan on 5/13/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import UIKit

@IBDesignable
class BorderButton: UIButton {
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var selectedColor: UIColor = UIColor.white
    @IBInspectable var disableColor: UIColor = UIColor.white
    fileprivate var normalColor: UIColor?
    
    override var isEnabled: Bool {
        didSet {
            if isEnabled {
                backgroundColor = normalColor
            } else {
                backgroundColor = disableColor
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        normalColor = backgroundColor
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        backgroundColor = selectedColor
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
        backgroundColor = normalColor
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        
        backgroundColor = normalColor
    }
}
