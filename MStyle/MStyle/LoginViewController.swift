//
//  LoginViewController.swift
//  MStyle
//
//  Created by luan on 8/1/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Alamofire

class LoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func loginTapped() {
        let login = FBSDKLoginManager()
        login.logIn(withReadPermissions: FBConstaint.permission, from: self) { [weak self] (result, error) in
            if error != nil || result == nil {
                print("Progress error")
            } else if (result?.isCancelled)! {
                print("Cancelled")
            } else {
                MStyleServer.logInWithFacebookToken(result!.token.tokenString, completion: {
                    result in
                    if result.isSuccess {
                        self?.loginSucceed()
                    }
                })
            }
        }
    }
    
    fileprivate func loginSucceed() {
        MStyleServer.getPhotoCount { count in
            showFirstPage = count == 0
            self.performSegue(withIdentifier: "LoginToHome", sender: nil)
        }
        
    }
}
