//
//  SortType.swift
//  MStyle
//
//  Created by luan on 9/4/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import Foundation

enum SortType: String {
    case date = "Date"
    case love = "Love"
    case random = "Random"
    
    static var values: [SortType] {
        return [.date, .love, .random]
    }
}
