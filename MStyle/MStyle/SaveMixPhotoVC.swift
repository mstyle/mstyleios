//
//  SaveMixPhotoVC.swift
//  MStyle
//
//  Created by luan on 10/1/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import UIKit

class SaveMixPhotoVC: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var loveButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    
    var photo: Photo!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.layer.cornerRadius = 8
        infoView.layer.cornerRadius = 4
        
        imageView.image = photo.image
    }
    
    func updateLoveUI() {
        loveButton.isSelected = photo.favorited
    }
    
    @IBAction func shareFacebook() {
        
    }
    
    @IBAction func back() {
        if let mixVC = storyboard?.instantiateViewController(withIdentifier: "MixVC") as? MixVC {
            mixVC.photo = photo
            mixVC.user = User.currentUser
            if var viewControllers = navigationController?.viewControllers {
                viewControllers.removeLast()
                viewControllers.append(mixVC)
                navigationController?.setViewControllers(viewControllers, animated: true)
            }
        }
    }
    
    @IBAction func love() {
        photo.favorited = !photo.favorited
        CoreDataManager.save()
        updateLoveUI()
    }
    
    @IBAction func goCloset() {
        if let tabBarController = tabBarController {
            tabBarController.selectedIndex = 0
            if let navigationController = tabBarController.viewControllers?[0] as? UINavigationController {
                navigationController.popToRootViewController(animated: false)
            }
        }
    }
}
