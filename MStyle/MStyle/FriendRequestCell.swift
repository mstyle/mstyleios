//
//  FriendRequestCell.swift
//  MStyle
//
//  Created by luan on 5/14/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import UIKit

class FriendRequestCell: UITableViewCell {

    weak var friend: User? {
        didSet {
            if let friend = friend {
                nameLabel.text = friend.fullName
                if let url = friend.avatarUrl {
                    avatarImageView.hnk_setImageFromURL(url)
                }
            }
        }
    }
    
    var last: Bool = false {
        didSet {
            lineView.isHidden = !last
        }
    }
    
    var acceptFriendHandle: ((Int) -> Void)?
    var deniedFriendHandle: ((Int) -> Void)?
    // MARK: Outlet
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var lineView: UIView!
    
    // MARK: Override TableViewCell
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: Action
    @IBAction func acceptFriend() {
        if let id = friend?.id {
            acceptFriendHandle?(id)
        }
    }
    @IBAction func deniedFriend() {
        if let id = friend?.id {
            deniedFriendHandle?(id)
        }
    }
}
