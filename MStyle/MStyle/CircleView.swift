//
//  CircleView.swift
//  MStyle
//
//  Created by luan on 6/24/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import UIKit

class CircleView: UIView {
    override var frame: CGRect {
        didSet {
            layer.cornerRadius = frame.width / 2
        }
    }
}
