//
//  CGSize.swift
//  MStyle
//
//  Created by luan on 10/14/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import Foundation
import UIKit

func *(left: CGSize, right: CGFloat) -> CGSize {
    return CGSize(width: left.width * right, height: left.height * right)
}

func /(left: CGSize, right: CGFloat) -> CGSize {
    guard right != 0.0 else {
        fatalError("Can't devine by 0")
    }
    return CGSize(width: left.width / right, height: left.height / right)
}
