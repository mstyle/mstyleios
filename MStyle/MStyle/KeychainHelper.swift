//
//  KeychainHelper.swift
//  MStyle
//
//  Created by luan on 8/8/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//
import Foundation

struct KeychainHelper {
    //fileprivate static let keychain = Keychain(service: KeychainConstaint.kKeychainService)
    
    static func setString(_ string: String, forKey key: String) {
        UserDefaults.standard.set(string, forKey: key)
        UserDefaults.standard.synchronize()
        //keychain[key] = string
    }
    
    static func getStringForKey(_ key: String) -> String? {
        return UserDefaults.standard.string(forKey: key)
        //return keychain[key]
    }
    
    static func clear() {
        //do {
        //    try keychain.removeAll()
        //} catch {
        //    print("Error when remove all keychain")
        //}
    }
}
