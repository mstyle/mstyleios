//
//  CaptureButton.swift
//  MStyle
//
//  Created by Phu Quang Nguyen on 5/22/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit

class CaptureButton: UIView {

    var lastLocation: CGPoint = CGPoint.zero {
        didSet {
            topConstraint.constant = lastLocation.y
            leftConstraint.constant = lastLocation.x
        }
    }
    var onPress: (() -> Void)? = nil
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var movingHeightConstraint: NSLayoutConstraint! {
        didSet {
            oldMovingConstraint = movingHeightConstraint.constant
        }
    }
    private var oldMovingConstraint: CGFloat!
    private var hadMove = false
    
    override var frame: CGRect {
        didSet {
            layer.cornerRadius = frame.width / 2
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(dragCaptureButton(_:)))
        self.addGestureRecognizer(panGestureRecognizer)
    }
    
    deinit {
        print("Memories Deinit \(self)")
    }

    func dragCaptureButton(_ panGestureRecognizer: UIPanGestureRecognizer) {
        let translation  = panGestureRecognizer.translation(in: self.superview)
        let currentLocation = CGPoint(x: lastLocation.x + translation.x, y: lastLocation.y + translation.y)
        let isOverXAxis = currentLocation.x < 0 || currentLocation.x > (self.superview!.frame.size.width - self.frame.size.width)
        let isOverYAxis = currentLocation.y < 0 || currentLocation.y > (self.superview!.frame.size.height - 55.0 - self.frame.size.height)
        
        if !(isOverXAxis || isOverYAxis) {
            let nextLocation = CGPoint(x: lastLocation.x + translation.x, y: lastLocation.y + translation.y)
            topConstraint.constant = nextLocation.y
            leftConstraint.constant = nextLocation.x
            hadMove = true
            animateOutMovingHeight()
        }
    }
    
    fileprivate func animateOutMovingHeight() {
        UIView.animate(withDuration: 0.5) {
            self.movingHeightConstraint.constant = self.oldMovingConstraint
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
     
        if !hadMove {
            onPress?()
            animateOutMovingHeight()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // Promote the touched view
        super.touchesBegan(touches, with: event)
        
        lastLocation = CGPoint(x: leftConstraint.constant, y: topConstraint.constant)
        hadMove = false
        
        UIView.animate(withDuration: 0.5) { 
            self.movingHeightConstraint.constant = self.frame.height
        }
        
        self.next?.touchesBegan(touches, with: event)
    }
}
