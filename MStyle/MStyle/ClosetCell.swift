//
//  ClosetCell.swift
//  MStyle
//
//  Created by luan on 8/24/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit

class ClosetCell: UICollectionViewCell {
    
    @IBOutlet weak var closetImageView: UIImageView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var deleteView: UIView!
    
    fileprivate weak var photo: Photo!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        closetImageView.layer.cornerRadius = 10
        closetImageView.clipsToBounds = true
        
        layer.cornerRadius = 4
    }
    
    override var isSelected: Bool {
        didSet {
            backgroundColor = isSelected ? UIColor(netHex: 0x29b1cb) : .clear
        }
    }
    
    var isChooseDelete: Bool = false {
        didSet {
            deleteView.isHidden = !isChooseDelete
        }
    }
    
    func showWith(photo: Photo) {
        self.photo = photo
        self.photo.delegate = self
        closetImageView.image = nil
        let boundsSize = closetImageView.bounds.size
        
        DispatchQueue(label: "Closet").async { [weak self] in
            guard self != nil else {
                return
            }
            
            if let image = photo.image?.imageScaleFit2(boundsSize * 2) {
                DispatchQueue.main.async { [weak self] in
                    self?.closetImageView.image = image
                    self?.activityIndicatorView.stopAnimating()
                }
            } else {
                DispatchQueue.main.async { [weak self] in
                    self?.closetImageView.image = nil
                    self?.activityIndicatorView.startAnimating()
                    self?.showDownload()
                }
            }
        }
    }
    
    func configuationWithPhoto(_ photo: Photo, editMode: PartClother = .none) {
        self.photo = photo
        closetImageView.image = nil
        
        if let _ = photo.image {
            showPhotoWithPart(editMode)
            activityIndicatorView.stopAnimating()
        } else {
            showDownload()
        }
    }
    
    func showPhotoWithPart(_ part: PartClother) {
        let closetSize = closetImageView.bounds.size
        let boundsSize = bounds.size
        DispatchQueue(label: "image").async { [weak self] in
            guard let sSelf = self else {
                return
            }
            guard let photo = sSelf.photo else {
                return
            }
            guard let image = photo.cacheImage(fitSize: closetSize * 2)else {
                return
            }
            
            let rect = image.rectWithPart(part: part)
            let scale = min(rect.size.width / boundsSize.width,
                            rect.size.height / boundsSize.height)
            let newWidth = image.size.width / scale
            let newHeight = image.size.height / scale
            let frame = CGRect(x: -(newWidth - boundsSize.width) / 2,
                               y: -rect.origin.y / scale,
                               width: newWidth,
                               height: newHeight)
            DispatchQueue.main.async { [weak self] in
                self?.closetImageView.image = image
                self?.closetImageView.frame = frame
            }
        }
    }
    
    fileprivate func showDownload() {
        activityIndicatorView.startAnimating()
        MStyleServer.downloadPhoto(photo)
    }
    
    static let kIdentifier = "ClosetCollectionCell"
}

extension ClosetCell: PhotoDelegate {
    func photoDidDownloadSuccessful(_ photo: Photo) {
        guard self.photo === photo else {
            return
        }
        
        showWith(photo: photo)
    }
    
    func photoDidDownloadFail(_ photo: Photo, error: Error) {
    }
}
