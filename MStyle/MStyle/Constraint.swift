//
//  Constraint.swift
//  MStyle
//
//  Created by luan on 8/1/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import Foundation
import UIKit

struct FBConstaint {
    static let permission = ["public_profile", "email", "user_friends"]
    static let publishActionPermission = "publish_actions"
}

struct KeychainConstaint {
    static let kKeychainService = "com.mstyle.authorization"
    
    static let kAccessTokenKey = "AccessToken"
    static let kRefreshTokenKey = "RefreshToken"
}

struct Color {
    static let kNavigationBackgroundColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0)
    static let kMixNavigationBackgroundColor = UIColor(red: 213.0/255.0, green: 115.0/255.0, blue: 126.0/255.0, alpha: 1.0)
}