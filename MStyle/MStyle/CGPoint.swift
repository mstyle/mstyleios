//
//  CGPoint.swift
//  MStyle
//
//  Created by Minh Luan Tran on 6/14/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import Foundation
import UIKit

func *(left: CGPoint, right: CGFloat) -> CGPoint {
    return CGPoint(x: left.x * right, y: left.y * right)
}

func /(left: CGPoint, right: CGFloat) -> CGPoint {
    return CGPoint(x: left.x / right, y: left.y / right)
}
