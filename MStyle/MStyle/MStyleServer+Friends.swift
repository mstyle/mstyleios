//
//  MStyleServer+Friends.swift
//  MStyle
//
//  Created by luan on 4/13/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import Foundation
import Alamofire

extension MStyleServer {
    
    // MARK: Get list friend
    
    static func getFriends(completion: @escaping (([User]) -> Void)) {
        let url = HOST + FRIENDS
        let request = Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
        request.validate().responseJSON { response in
            var users: [User] = []
            if case .success(let value) = response.result, let datas = value as? [[String: AnyObject]] {
                for data in datas {
                    if let user = User(dictionary: data) {
                        users.append(user)
                    }
                }
            }
            completion(users)
        }
    
    }
    
    static func getFriendsSuggesting(completion: @escaping (([User]) -> Void)) {
        let url = HOST + FRIENDS_SUGGESTING
        let request = Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
        request.validate().responseJSON { response in
            var users: [User] = []
            if case .success(let value) = response.result, let datas = value as? [[String: AnyObject]]
            {
                for data in datas {
                    if let user = User(dictionary: data) {
                        users.append(user)
                    }
                }
            }
            completion(users)
        }
    }
    
    static func getFriendsRequest(completion: @escaping (([User]) -> Void)) {
        let url = HOST + FRIENDS_REQUEST
        let request = Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
        request.validate().responseJSON { response in
            var users: [User] = []
            if case .success(let value) = response.result, let datas = value as? [[String: AnyObject]]
            {
                for data in datas {
                    if let user = User(dictionary: data) {
                        users.append(user)
                    }
                }
                completion(users)
            }
        }
    }
    
    // MARK: Friend request action
    
    static func requestFriend(_ friendId: Int, completion: ((Bool) -> Void)? = nil) {
        let url = HOST + String(format: FRIEND_REQUEST_ACTION, friendId)
        let request = Alamofire.request(url, method: .post, parameters: nil, encoding: URLEncoding.default, headers: headers)
        request.validate().response {
            response in
            
            completion?(response.error == nil)
        }
    }
    
    static func acceptFriend(_ friendId: Int, completion: ((Bool) -> Void)? = nil) {
        let url = HOST + String(format: FRIEND_ACCEPT_ACTION, friendId)
        let request = Alamofire.request(url, method: .post, parameters: nil, encoding: URLEncoding.default, headers: headers)
        request.validate().response {
            response in
            
            completion?(response.error == nil)
        }
    }
    
    static func denyFriend(_ friendId: Int, completion: ((Bool) -> Void)? = nil) {
        let url = HOST + String(format: FRIEND_DENY_ACTION, friendId)
        let request = Alamofire.request(url, method: .post, parameters: nil, encoding: URLEncoding.default, headers: headers)
        request.validate().response {
            response in
            
            completion?(response.error == nil)
        }
    }
    
    // MARK: Friend Photo
    
    static func getAllPhotoFriend(id friendId: Int, completion: @escaping (([Photo]) -> Void)) -> Request {
        let url = HOST + String(format: FRIEND_PHOTOS, friendId) + "?sort=createdDate,desc"
        let request = Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
        request.validate().responseJSON {
            response in
            
            guard let context = CoreDataManager.context else {
                return
            }
            var photos: [Photo] = []
            if case .success(let value) = response.result, let dictionaries = value as? [[String: AnyObject]] {
                for dictionary in dictionaries {
                    if let photo = Photo(dictionary: dictionary, context: context, needSave: false) {
                        photos.append(photo)
                    }
                }
            }
            completion(photos)
        }
        return request
    }
    
}
