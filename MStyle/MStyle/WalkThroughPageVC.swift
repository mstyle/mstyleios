//
//  WalkThroughPageVC.swift
//  MStyle
//
//  Created by luan on 4/28/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import UIKit

class WalkThroughPageVC: UIPageViewController {
    
    fileprivate lazy var orderViewControllers: [UIViewController] = {
       return [viewController(withIdentifier: "WalkThough01"),
               viewController(withIdentifier: "WalkThough02"),
               viewController(withIdentifier: "WalkThough03"),
               viewController(withIdentifier: "WalkThough04")]
    }()
    
    // MARK: View Controller

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        dataSource = self
        
        if let firstViewController = orderViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
    }
}

extension WalkThroughPageVC: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let curIndex = orderViewControllers.index(of: viewController) else {
            return nil
        }
        
        guard curIndex >= 0 && curIndex < orderViewControllers.count - 1 else {
            return nil
        }
        
        return orderViewControllers[curIndex + 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let curIndex = orderViewControllers.index(of: viewController) else {
            return nil
        }
        
        guard curIndex > 0 && curIndex < orderViewControllers.count else {
            return nil
        }
        
        return orderViewControllers[curIndex - 1]
    }
}
