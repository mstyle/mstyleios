//
//  ClosetCollectionLayout.swift
//  MStyle
//
//  Created by luan on 5/24/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import UIKit

class ClosetCollectionLayout: UICollectionViewLayout {

    fileprivate var cacheAttributes = [UICollectionViewLayoutAttributes]()
    fileprivate var sizeSmall: CGFloat {
        return (sizeLarge - 1.0) * 0.5
    }
    fileprivate var sizeLarge: CGFloat {
        return (collectionView!.bounds.width -  3) * 0.5
    }
    
    fileprivate func xOffsetAt(index: Int) -> CGFloat {
        switch index % 10 {
        case 0, 5, 7:
            return 1.0
        case 1, 3, 9:
            return 2.0 + sizeLarge
        case 2, 4:
            return 3.0 + sizeLarge + sizeSmall
        case 6, 8:
            return 2.0 + sizeSmall
        default:
            return 1.0
        }
    }
    fileprivate func yOffsetAt(index: Int) -> CGFloat {
        var yOffset = CGFloat(index / 10) * (2 * sizeLarge + 2)
        switch index % 10 {
        case 0, 1, 2:
            yOffset += 1.0
        case 3, 4:
            yOffset += 2.0 + sizeSmall
        case 5, 6, 9:
            yOffset += 2.0 + sizeLarge
        case 7, 8:
            yOffset += 3.0 + sizeLarge + sizeSmall
        default:
            yOffset += 1.0
        }
        return yOffset
    }
    fileprivate func sizeAt(index: Int) -> CGFloat {
        switch index % 10 {
        case 0, 9:
            return sizeLarge
        default:
            return sizeSmall
        }
    }
    
    var didLoad = false
    fileprivate var height: CGFloat = 0.0
    
    override var collectionViewContentSize : CGSize {
        return CGSize(width: collectionView!.bounds.width, height: height)
    }
    
    override func prepare() {
        guard !didLoad else {
            return
        }
        
        height = 1.0
        cacheAttributes = [UICollectionViewLayoutAttributes]()
        for section in 0..<collectionView!.numberOfSections {
            for item in 0..<collectionView!.numberOfItems(inSection: section) {
                let indexPath = IndexPath(item: item, section: section)
                let attribute = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                
                let x = xOffsetAt(index: item)
                let y = yOffsetAt(index: item)
                let size = sizeAt(index: item)
                attribute.frame = CGRect(x: x, y: y, width: size, height: size)
                
                height = max(height, y + size)
                cacheAttributes.append(attribute)
            }
        }
        didLoad = true
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cacheAttributes[indexPath.row]
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes = [UICollectionViewLayoutAttributes]()
        for attributes in cacheAttributes {
            if attributes.frame.intersects(rect) {
                layoutAttributes.append(attributes)
            }
        }
        
        return layoutAttributes
    }
}
