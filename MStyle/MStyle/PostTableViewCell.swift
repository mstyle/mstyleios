//
//  PostTableViewCell.swift
//  MStyle
//
//  Created by luan on 10/8/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit
import Alamofire
import Haneke

class PostTableViewCell: UITableViewCell {
    
    // MARK: Variable
    var widthImage: CGFloat {
        return bounds.width - 20
    }
    
    var heightImage: CGFloat {
        get {
            var ratio: CGFloat = 1.0
            if let photo = post.photo {
                ratio = CGFloat(photo.width) / CGFloat(photo.height)
            } else if let width = post.photoWidth, let height = post.photoHeight {
                ratio = CGFloat(width) / CGFloat(height)
            }
            return widthImage / ratio
        }
    }
    
    var imageSize: CGSize {
        get {
            return CGSize(width: widthImage, height: heightImage)
        }
    }
    
    var post: Post! {
        didSet {
            nameLabel.text = post.user?.fullName
            nameDiscussionLabel.text = nameLabel.text
            discussionLabel.text = post.discussion
            timeLabel.text = post.date
            
            if let url = post.user?.avatarUrl {
                avatarImageView.hnk_setImageFromURL(url)
            }
            
            photoImageView.image = nil
            if let post = post {
                loadingIndicatorView.startAnimating()
                post.getImage(imageSize) { [weak self] image in
                    if self == nil || post !== self?.post {
                        return
                    }
                    
                    self!.photoImageView.image = image
                    self!.loadingIndicatorView.stopAnimating()
                }
            }
        }
    }
    // MARK: Oulet
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var loadingIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet weak var nameDiscussionLabel: UILabel!
    @IBOutlet weak var discussionLabel: UILabel!
    
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var likeLabel: UILabel!
    
    // MARK: Override Cell

    override func awakeFromNib() {
        super.awakeFromNib()
        
        photoImageView.layer.masksToBounds = true
        //photoImageView.layer.borderColor = UIColor.darkGrayColor().CGColor
        //photoImageView.layer.borderWidth = 1
        photoImageView.layer.cornerRadius = 8
    }
    
    // MARK: Action
    
    @IBAction func likeTapped() {
        MStyleServer.likePost(post) { [weak self] (success, id) in
            guard let postId = self?.post.id else {
                return
            }
            if success && id == postId {
                self?.likeButton.isSelected = true
            }
        }
    }
}
