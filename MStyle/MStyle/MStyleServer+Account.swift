//
//  MStyleServer+Account.swift
//  MStyle
//
//  Created by luan on 12/22/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import Foundation
import Alamofire

// MARK: Authorization

extension MStyleServer {
    static func logInWithFacebookToken(_ token: String, completion: ((_ result: Result<Any>) -> Void)? = nil) {
        let parameters = [
            AuthorizationKey.providerId: "facebook",
            AuthorizationKey.fbAccessToken: token,
            AuthorizationKey.grant_type: "social",
            AuthorizationKey.client_id: CLIENT_ID,
            AuthorizationKey.client_secret: CLIENT_SECRET
        ]
        let URL = HOST + LOG_IN_END_POINT
        
        let request = Alamofire.request(URL, method: HTTPMethod.post, parameters: parameters, encoding: URLEncoding.queryString, headers: loginHeaders)
        request.validate().responseJSON { response in
            
            if case Result.success(let value) = response.result, let data = value as? [String: Any] {
                if let access_token = data[AuthorizationKey.access_token] as? String {
                    KeychainHelper.setString(access_token, forKey: KeychainConstaint.kAccessTokenKey)
                }
                if let refresh_token = data[AuthorizationKey.refresh_token] as? String {
                    KeychainHelper.setString(refresh_token, forKey: KeychainConstaint.kRefreshTokenKey)
                }
                if let userId = data[UserDefaultsKey.userId.string] as? Int {
                    UserDefaultsHelper.setInteger(userId, forKey: UserDefaultsKey.userId)
                    UserDefaultsHelper.synchronize()
                }
            } else if case Result.failure(let error) = response.result {
                print("log in with FB token error " + error.localizedDescription)
            }
            completion?(response.result)
        }
    }
    
    static func isAuthenticated(_ completion: ((DataResponse<Data>) -> Void)? = nil) {
        let isAuthenticatedURL = HOST + IS_AUTHENTICATED_END_POINT
        
        let request = Alamofire.request(isAuthenticatedURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
        request.validate().responseData { response in
            print("====================================")
            print("isAuthenticated")
            print("====================================")
            print("\(response.result)")
            print("====================================")
            completion?(response)
        }
    }
    
    static func getNewAccessToken(_ completion: ((DataResponse<Any>?) -> Void)? = nil) {
        guard let refreshToken = KeychainHelper.getStringForKey(KeychainConstaint.kRefreshTokenKey) else {
            completion?(nil)
            return
        }
        
        let parameters = [
            AuthorizationKey.refresh_token: refreshToken,
            AuthorizationKey.grant_type: "refresh_token",
            AuthorizationKey.client_id: CLIENT_ID,
            AuthorizationKey.client_secret: CLIENT_SECRET
        ]
        let URL = HOST + LOG_IN_END_POINT
        
        let request = Alamofire.request(URL, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: loginHeaders)
        request.validate().responseJSON { response in
            if case Result.success(let value) = response.result, let data = value as? [String: Any] {
                if let access_token = data[AuthorizationKey.access_token] as? String {
                    KeychainHelper.setString(access_token, forKey: KeychainConstaint.kAccessTokenKey)
                }
                if let refresh_token = data[AuthorizationKey.refresh_token] as? String {
                    KeychainHelper.setString(refresh_token, forKey: KeychainConstaint.kRefreshTokenKey)
                }
                print("====================================")
                print("getNewAccessToken")
                print("====================================")
                print("\(response.result)")
                print("====================================")
            }
            completion?(response)
        }
    }
    
    static func hadAccessToken() -> Bool {
        if let accessToken = KeychainHelper.getStringForKey(KeychainConstaint.kAccessTokenKey), !accessToken.isEmpty {
            print("access TOken : \(accessToken)")
            return true
        } else {
            return false
        }
    }
}

// MARK: Account
extension MStyleServer {
    static func getCurrentAccount() {
        let url = HOST + CURRENT_ACCOUNT
        
        let request = Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
        request.validate().responseJSON { response in
            print("====================================")
            print("getCurrentAccount")
            print("====================================")
            print("\(response.result)")
            print("====================================")
            if case .success(let value) = response.result {
                if let dictionaries = value as? [String : AnyObject]  {
                    User.currentUser.initWithDictionary(dictionaries)
                }
            }
            print("====================================")
        }
    }
}
