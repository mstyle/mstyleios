//
//  MixScrollView.swift
//  MStyle
//
//  Created by luan on 11/5/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit

protocol MixScrollViewDelegate: class {
    func didTappedMixScrollView(_ mixScrollView: MixScrollView)
    func didMixScrollViewScrollIndex(_ mixScrollView: MixScrollView, index: Int)
}

class MixScrollView: UIScrollView {
    fileprivate let imageScale: CGFloat = 1.0
    
    fileprivate var sizeImage: CGSize {
        get {
            return sizeContent * imageScale
        }
    }
    var sizeContent = CGSize.zero
    var index: Int = -1
    fileprivate var currentPage = 0
    
    var photos = [Photo]()
    fileprivate var imageView = UIImageView()
    
    fileprivate lazy var tapGesture: UITapGestureRecognizer = {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tap(_:)))
        tapGesture.cancelsTouchesInView = false
        return tapGesture
    }()
    
    fileprivate var nextButton = UIButton()
    fileprivate var prevButton = UIButton()
    
    weak var mixDelegate: MixScrollViewDelegate?
    
    var canNext: Bool {
        get {
            return index < photos.count - 1
        }
    }
    var canPrev: Bool {
        get {
            return index > 0
        }
    }
    
    // MARK: Override
    
    override var frame: CGRect {
        didSet {
            if oldValue == CGRect.zero {
                contentOffset = CGPoint(x: (sizeImage.width - sizeContent.width) / 2,
                                        y: (sizeImage.height - sizeContent.height) / 2 + frame.origin.y)
            } else {
                contentOffset = CGPoint(x: contentOffset.x,
                                        y: frame.origin.y - oldValue.origin.y + contentOffset.y)
            }
            
            contentSize = sizeImage
            imageView.frame = CGRect(origin: CGPoint.zero, size: contentSize)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        showsVerticalScrollIndicator = false
        showsHorizontalScrollIndicator = false
        isPagingEnabled = false
        backgroundColor = .white
        delegate = self
        minimumZoomScale = 1.0
        maximumZoomScale = 1.2
        bouncesZoom = false
        
        addSubview(imageView)
        addGestureRecognizer(tapGesture)
        
        addSubview(nextButton)
        addSubview(prevButton)
        
        let xNextConstraint = NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: nextButton, attribute: .trailing, multiplier: 1.0, constant: 8)
        let yNextConstraint = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: nextButton, attribute: .centerY, multiplier: 1.0, constant: 0)
        
        let xPrevConstraint = NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: prevButton, attribute: .leading, multiplier: 1.0, constant: 8)
        let yPrevConstraint = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: prevButton, attribute: .centerY, multiplier: 1.0, constant: 0)
        
        nextButton.translatesAutoresizingMaskIntoConstraints = false
        prevButton.translatesAutoresizingMaskIntoConstraints = false
        addConstraints([xNextConstraint, yNextConstraint, xPrevConstraint, yPrevConstraint])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        
        if newSuperview == nil {
            removeGestureRecognizer(tapGesture)
        }
    }
    
    // MARK: GestureRecognier
    func tap(_ sender: UITapGestureRecognizer) {
        mixDelegate?.didTappedMixScrollView(self)
    }
    
    // MARK: Show
    func initWithIndex(_ index: Int) {
        if self.index == index {
            return
        }
        
        self.index = index
        if index >= photos.count {
            return
        }
        
        
        loadImageAtIndex(index)
    }
    
    func showWithFrame(_ frame: CGRect) {
        self.frame = frame
        isHidden = false
    }
    
    fileprivate func loadImageAtIndex(_ index: Int) {
        if index < 0 || index >= photos.count {
            return
        }
        
        if index == self.index {
            imageView.image = photos[index].image
        }
    }
    
    // MARK: Swipe
    func nextMixPhoto() {
        if !canNext {
            return
        }
        
        index += 1
        loadImageAtIndex(index)
        
        mixDelegate?.didMixScrollViewScrollIndex(self, index: index)
    }
    
    func prevMixPhoto() {
        if !canPrev {
            return
        }
        
        index -= 1
        loadImageAtIndex(index)
        
        mixDelegate?.didMixScrollViewScrollIndex(self, index: index)
    }
}

// MARK:
// MARK: UIScrollViewDelegate
extension MixScrollView: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
