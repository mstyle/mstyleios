//
//  SavePhotoVC.swift
//  MStyle
//
//  Created by luan on 3/1/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import UIKit

class SavePhotoVC: UIViewController {

    @IBOutlet weak var saveMixButton: UIButton!
    @IBOutlet weak var saveGoClosetButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    
    var image: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageView.image = image
        imageView.layer.cornerRadius = 8
        saveMixButton.layer.cornerRadius = 21
        saveGoClosetButton.layer.cornerRadius = 21
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == Segues.goMix {
            if let mixVC = segue.destination as? MixVC {
                mixVC.user = User.currentUser
                mixVC.photo = sender as? Photo
            }
        }
    }
    
    @discardableResult
    func createPhoto() -> Photo? {
        let photo = CoreDataManager.createPhotoWithImage(image,
                                                         separator1: Int(image.size.height * 0.333),
                                                         separator2: Int(image.size.height * 0.75))
        if let photo = photo {
            User.currentUser.addPhoto(photo)
            CoreDataManager.save()
            photo.uploadServerIfNeed()
        }
        return photo
    }
    
    @IBAction func back() {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveAndMix() {
        if let photo = createPhoto() {
            performSegue(withIdentifier: Segues.goMix, sender: photo)
        }
    }
    
    @IBAction func saveAndGoCloset() {
        createPhoto()
        if let tabBarController = tabBarController {
            tabBarController.selectedIndex = 0
            if let navigationController = tabBarController.viewControllers?[0] as? UINavigationController {
                navigationController.popToRootViewController(animated: false)
            }
        }
    }
    
    struct Segues {
        static let goMix = "goMix"
    }
}
