//
//  AddTagCell.swift
//  MStyle
//
//  Created by luan on 7/29/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit

@IBDesignable
class AddTagCell: UICollectionViewCell {
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBInspectable var borderColor: UIColor?
    @IBInspectable var cornerRadius: CGFloat = 0
    @IBInspectable var borderWidth: CGFloat = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        view.layer.cornerRadius = cornerRadius
        view.layer.borderColor = borderColor?.cgColor
        view.layer.borderWidth = borderWidth
        
        //view.layer.cornerRadius = 12
        //view.layer.borderColor = UIColor(red: 167.0/255.0, green: 167.0/255.0, blue: 167.0/255.0, alpha: 1.0).CGColor
        //view.layer.borderWidth = 2
    }
    
    static let kIdentifier = "AddTagCell"
}
