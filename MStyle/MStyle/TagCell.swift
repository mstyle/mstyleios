//
//  TagCell.swift
//  MStyle
//
//  Created by luan on 7/29/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit

protocol TagCellDelegate: class {
    func deleteCell(_ cell: UICollectionViewCell)
}

class TagCell: UICollectionViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var view: UIView!
    
    weak var delegate: TagCellDelegate? = nil
    
    @IBInspectable var borderColor: UIColor?
    @IBInspectable var cornerRadius: CGFloat = 0
    @IBInspectable var borderWidth: CGFloat = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //view.layer.cornerRadius = 12
        //view.layer.borderColor = UIColor(red: 167.0/255.0, green: 167.0/255.0, blue: 167.0/255.0, alpha: 1.0).CGColor
        //view.layer.borderWidth = 2
        view.layer.cornerRadius = cornerRadius
        view.layer.borderColor = borderColor?.cgColor
        view.layer.borderWidth = borderWidth
        
        deleteButton.addTarget(self, action: #selector(deleteTag(_:)), for: .touchUpInside)
    }
    
    func updateUI(_ tag: String) {
        nameLabel.text = tag
    }
    
    func deleteTag(_ sender: UIButton) {
        delegate?.deleteCell(self)
    }
    
    static let kIdentifier = "TagCell"
}
