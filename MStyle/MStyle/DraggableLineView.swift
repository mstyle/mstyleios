//
//  DraggableLineImageView.swift
//  MStyle
//
//  Created by luan on 7/31/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit

protocol DragLineDelegate: class {
    func dragLineDidMove(_ dragLine: DraggableLineView)
}

class DraggableLineView: UIView {

    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var bodyView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint?
    fileprivate var location: CGPoint!
    weak var delegate: DragLineDelegate?
    var targetImageHeight: CGFloat = 0
    
    fileprivate var oldValue = -1
    var value: Int {
        get {
            let yOffset = center.y - contentView.frame.origin.y
            let contentHeight = contentView.frame.height
            
            
            return Int(yOffset * targetImageHeight / contentHeight)
        }
        set {
            if newValue != oldValue {
                oldValue = newValue
                updateValueWithHeight(CGFloat(newValue), height: targetImageHeight)
            }
        }
    }
    
    var valuePercent: CGFloat {
        get {
            return (center.y - contentView.frame.origin.y) / contentView.frame.height
        }
    }
    
    var highlighted = false {
        didSet {
            if highlighted {
                
            } else {
                lineView.backgroundColor = UIColor.white
                bodyView.backgroundColor = UIColor.white
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(dragImageView(_:)))
        addGestureRecognizer(panGestureRecognizer)
        
        location = CGPoint(x: frame.origin.x + frame.width / 2, y: frame.origin.y + frame.height / 2)
        bodyView.layer.cornerRadius = 4
    }
    
    fileprivate func updateValueWithHeight(_ value: CGFloat, height: CGFloat) {
        center.y = CGFloat(value) * contentView.frame.height / height + contentView.frame.origin.y
        topConstraint?.constant = center.y - bounds.height / 2
        delegate?.dragLineDidMove(self)
    }
    
    func dragImageView(_ panGestureRecognizer: UIPanGestureRecognizer) {
        let translation  = panGestureRecognizer.translation(in: contentView)
        let currentLocation = CGPoint(x: location.x, y: location.y + translation.y)
        
        switch panGestureRecognizer.state {
        case .began, .changed:
            highlighted = true
        default:
            highlighted = false
        }
        
        if currentLocation.y >= 0 && currentLocation.y <= contentView.frame.height {
            self.center = currentLocation
            topConstraint?.constant = center.y - bounds.height / 2
            
            let yOffset = center.y - contentView.frame.origin.y
            let contentHeight = contentView.frame.height
            
            value = Int(yOffset * targetImageHeight / contentHeight)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        highlighted = true
        location = CGPoint(x: frame.origin.x + frame.width / 2, y: frame.origin.y + frame.height / 2)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
    
        highlighted = false
    }
}
