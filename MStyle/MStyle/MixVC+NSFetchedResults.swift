//
//  MixVC+NSFetchedResults.swift
//  MStyle
//
//  Created by luan on 10/22/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit
import CoreData

extension MixVC: NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            closetCollectionView.insertItems(at: [newIndexPath!])
        case .delete:
            closetCollectionView.deleteItems(at: [indexPath!])
        case .update:
            break;
        case .move:
            break;
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    }
}
