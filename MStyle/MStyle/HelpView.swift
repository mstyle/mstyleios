//
//  HelpView.swift
//  MStyle
//
//  Created by luan on 6/25/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import UIKit

@IBDesignable
class HelpView: UIView {
    
    override var frame: CGRect {
        didSet {
            let denta = frame.size.height / 2
            let mask = CAShapeLayer()
            mask.frame = self.layer.bounds
            
            let path = CGMutablePath()
            path.move(to: .zero)
            path.addLine(to: CGPoint(x: frame.width - denta, y: 0))
            path.addLine(to: CGPoint(x: frame.width, y: denta))
            path.addLine(to: CGPoint(x: frame.width - denta, y: frame.height))
            path.addLine(to: CGPoint(x: 0, y: frame.height))
            path.addLine(to: CGPoint(x: 0, y: 0))
            
            mask.path = path
            layer.mask = mask
            
            let shape = CAShapeLayer()
            shape.frame = bounds
            shape.path = path
            shape.fillColor = backgroundColor?.cgColor
            
            layer.insertSublayer(shape, at: 0)
        }
    }
}
