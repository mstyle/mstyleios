//
//  User.swift
//  MStyle
//
//  Created by luan on 10/8/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import Foundation
import Alamofire

class User {
    var id: Int
    var avatarPath: String?
    var fullName: String?
    var avatarUrl: URL? {
        get {
            return avatarPath != nil ? URL(string: avatarPath!) : nil
        }
    }
    var email: String?
    var firstName: String?
    
    var lastName: String?
    var login: String?
    
    var numPhoto: Int = 0
    
    var photos: [Photo] = [] {
        didSet {
            downloadPhotos()
            for photo in photos {
                photo.owner = self
            }
        }
    }
    weak var delegate: UserDelegate?
    
    init(id: Int) {
        self.id = id
    }
    
    init?(dictionary: [String: AnyObject]) {
        guard let userId = dictionary[Key.id] as? Int else {
            return nil
        }
        
        id = userId
        initWithDictionary(dictionary)
    }
    
    func initWithDictionary(_ dictionary: [String: AnyObject]) {
        avatarPath = dictionary[Key.avatarUrl] as? String
        fullName = dictionary[Key.fullName] as? String
        
        email = dictionary[Key.email] as? String
        firstName = dictionary[Key.firstName] as? String
        lastName = dictionary[Key.lastName] as? String
        login = dictionary[Key.login] as? String
        
        delegate?.didUpdateInfo(user: self)
    }
    
    // MARK: Photo
    func addPhoto(_ photo: Photo) {
        photos.append(photo)
        photo.owner = self
        
        delegate?.didAddPhoto(photo: photo, user: self)
    }
    
    func photo(withId id: Int) -> Photo? {
        for photo in photos {
            if photo.id?.intValue == id {
                return photo
            }
        }
        return nil
    }
    
    func getAllPhoto() {
        _ = MStyleServer.getAllPhotoFriend(id: id) { [weak self] photos in
            guard let sSelf = self else {
                return
            }
            sSelf.photos = photos
            sSelf.delegate?.didGetAllPhoto(user: sSelf)
        }
    }
    
    func downloadPhotos() {
        for photo in photos {
            if photo.image == nil {
                MStyleServer.downloadPhoto(photo, completion:
                {
                    [weak self, weak photo] success in
                    
                    if success, let photo = photo, let sSelf = self {
                        sSelf.delegate?.didDownload(photo: photo, user: sSelf)
                    }
                })
            }
        }
    }
    
    // MARK: Current User
    static var currentUser: User = {
        let user = User(id: UserDefaultsHelper.integerForKey(.userId))
        return user
    }()
    
    static func getCurrentUserData() {
        MStyleServer.getCurrentAccount()
        
        currentUser.photos = CoreDataManager.photos ?? []
        MStyleServer.getAllPhotos {
            (response: DataResponse<Any>) in
            
            if case .success(let value) = response.result, let dictionaries = value as? [[String: AnyObject]] {
                let user = User.currentUser
                var needSave = false
                
                for dictionary in dictionaries {
                    guard let id = dictionary[Photo.Key.kId] as? Int else {
                        continue
                    }
                    
                    if user.photo(withId: id) != nil {
                        continue
                    }
                    
                    if let photo = CoreDataManager.createPhotoWithDictionary(dictionary) {
                        user.addPhoto(photo)
                        needSave = true
                    }
                }
                
                if needSave {
                    CoreDataManager.save()
                }
            }
        }
    }
    
    // MARK: Key
    fileprivate struct Key {
        static let activated = "activated"
        static let email = "email"
        static let firstName = "firstName"
        static let fullName = "fullName"
        static let id = "id"
        static let langKey = "langKey"
        static let lastName = "lastName"
        static let login = "login"
        static let resetDate = "resetDate"
        static let resetKey = "resetKey"
        static let avatarUrl = "avatarUrl"
    }
}

protocol UserDelegate: class {
    func didUpdateInfo(user: User)
    func didGetAllPhoto(user: User)
    func didDownload(photo: Photo, user: User)
    func didAddPhoto(photo: Photo, user: User)
}
