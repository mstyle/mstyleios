//
//  Photo.swift
//  MStyle
//
//  Created by luan on 8/8/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import CoreData

enum PhotoMode: String {
    case friend = "FRIEND"
    case only = "PRIVATE"
}

enum PhotoType: String {
    case full = "full"
    case mixed = "mixed"
}

class Photo: NSManagedObject {
    // MARK: - Constraint
    static let entityName = "Photo"
    struct Key {
        static let kContentType = "contentType"
        static let kHeight = "height"
        static let kId = "id"
        static let kSeparator1 = "separator1"
        static let kSeparator2 = "separator2"
        static let kSeparator3 = "separator3"
        static let kSize = "size"
        static let kType = "type"
        static let kUserId = "userId"
        static let kWidth = "width"
        static let kCreatedDate = "createdDate"
        static let kLikeUsers = "likeUsers"
        static let kMode = "mode"
        
        static let kLocalId = "localId"
    }
    
    // MARK: Variables
    var type: PhotoType {
        get {
            return PhotoType(rawValue: typeString) ?? PhotoType.full
        }
        set {
            typeString = type.rawValue
        }
    }
    var mode: PhotoMode {
        get {
            return PhotoMode(rawValue: modeString) ?? PhotoMode.friend
        }
        set {
            modeString = newValue.rawValue
        }
    }
    var data: Data? {
        get {
            if let image = image {
                return UIImageJPEGRepresentation(image, 0.8)
            } else {
                return nil
            }
        }
    }
    var image: UIImage? {
        get {
            if let photoPath = photoPath {
                return UIImage(contentsOfFile: photoPath)
            } else {
                return nil
            }
        }
    }
    var cacheImage: UIImage?
    
    var downloadRequest: DownloadRequest?
    weak var delegate: PhotoDelegate?
    
    var uploading = false
    var likeUsers = Set<Int>()
    weak var owner: User?
    
    // MARK: Override NSManagedObject
    override func awakeFromFetch() {
        super.awakeFromFetch()
        
        uploadServerIfNeed()
        postWallIfNeed()
    }
    
    // MARK: Init
    convenience init?(dictionary: [String: AnyObject], context: NSManagedObjectContext, needSave: Bool = true) {
        guard let width = dictionary[Key.kWidth] as? Int else {
            return nil
        }
        guard let height = dictionary[Key.kHeight] as? Int else {
            return nil
        }
        guard let separator1 = dictionary[Key.kSeparator1] as? Int else {
            return nil
        }
        guard let separator2 = dictionary[Key.kSeparator2] as? Int else {
            return nil
        }
        guard let id = dictionary[Key.kId] as? Int else {
            return nil
        }
        guard let userId = dictionary[Key.kUserId] as? Int else {
            return nil
        }
        guard let contentType = dictionary[Key.kContentType] as? String else {
            return nil
        }
        guard let size = dictionary[Key.kSize] as? Int else {
            return nil
        }
        guard let typeString = dictionary[Key.kType] as? String else {
            return nil
        }
        guard let createdDateString = dictionary[Key.kCreatedDate] as? String else {
            return nil
        }
        guard let createdDate = dateFromString(createdDateString) else {
            return nil
        }
        guard let modeString = dictionary[Key.kMode] as? String else {
            return nil
        }
        
        guard let entity = NSEntityDescription.entity(forEntityName: Photo.entityName, in: context) else {
            return nil
        }
        
        if needSave {
            self.init(entity: entity, insertInto: context)
        } else {
            self.init(entity: entity, insertInto: nil)
        }
        
        self.width = NSNumber(value: width)
        self.height = NSNumber(value: height)
        self.separator1 = NSNumber(value: separator1)
        self.separator2 = NSNumber(value: separator2)
        if let separator3 = dictionary[Key.kSeparator3] as? Int {
            self.separator3 = NSNumber(value: separator3)
        }
        self.id = id as NSNumber
        self.userId = userId as NSNumber
        self.contentType = contentType
        self.size = NSNumber(value: size)
        self.typeString = typeString
        self.createdDate = createdDate
        self.modeString = modeString
        for likeUsers in (dictionary[Key.kLikeUsers] as? [Any]) ?? [] {
            if let id = (likeUsers as? [String: Int])?[Key.kUserId] {
                self.likeUsers.insert(id)
            }
        }
    }
    
    convenience init?(image: UIImage, separator1: Int, separator2: Int, type: PhotoType, context: NSManagedObjectContext) {
        guard let entity = NSEntityDescription.entity(forEntityName: Photo.entityName, in: context) else {
            return nil
        }
        
        self.init(entity: entity, insertInto: context)
        self.id = nil
        self.userId = nil
        self.localId = NSNumber(value: Photo.nextLocalPhotoID())
        
        self.height = NSNumber(value: Int(image.size.height))
        self.width = NSNumber(value: Int(image.size.width))
        self.separator1 = NSNumber(value: separator1)
        self.separator2 = NSNumber(value: separator2)
        
        self.contentType = "image/jpeg"
        self.size = NSNumber(value: image.Bytes)
        self.typeString = type.rawValue
        self.saveImage(image)
        self.createdDate = Date()
        self.modeString = PhotoMode.friend.rawValue
    }
    
    var dictionary: [String: AnyObject] {
        var dictionary : [String: AnyObject] = [
            Key.kContentType: contentType as AnyObject,
            Key.kHeight: height,
            Key.kSeparator1: separator1,
            Key.kSeparator2: separator2,
            Key.kSize: size,
            Key.kType: type.rawValue as AnyObject,
            Key.kWidth: width,
            Key.kMode: modeString as AnyObject
        ]
        if let id = id {
            dictionary[Key.kId] = id
        }
        if let separator3 = separator3 {
            dictionary[Key.kSeparator3] = separator3
        }
        if let userId = userId {
            dictionary[Key.kUserId] = userId
        }
        
        return dictionary
    }
    
    func updateUserInfo(_ dictionary: [String: AnyObject]) {
        if let userId = dictionary[Key.kUserId] as? Int {
            self.userId = NSNumber(value: userId)
        } else {
            self.userId = nil
        }
        if let id = dictionary[Key.kId] as? Int {
            self.id = NSNumber(value: id)
        } else {
            self.id = nil
        }
    }
    
    // MARK: Mix helper
    
    var minSeparator: Int {
        let minSeparator = min(Int(separator1), Int(separator2))
        
        return (separator3 != nil && Int(separator3!) > 0) ? min(Int(minSeparator), Int(separator3!)) : minSeparator
    }
    
    var maxSeparator: Int {
        let maxSeparator = max(Int(separator1), Int(separator2))
        return (separator3 != nil && Int(separator3!) > 0) ? max(maxSeparator, Int(separator3!)) : Int(maxSeparator)
    }
    
    func cacheImage(fitSize: CGSize) -> UIImage? {
        if let cacheImage = cacheImage {
            return cacheImage
        }
        cacheImage = image?.imageScaleFit2(fitSize)
        return cacheImage
    }
    
    // MARK: Download Photo
    func downloadedPhoto(_ error: Error?) {
        downloadRequest = nil
        if let error = error {
            delegate?.photoDidDownloadFail(self, error: error)
        } else {
            delegate?.photoDidDownloadSuccessful(self)
        }
    }
    
    // MARK: Post on wall
    func postWallIfNeed() {
        if let discussion = discussion, let id = id, !discussion.isEmpty {
            MStyleServer.createPost(discussion, photoId: Int(id), completion: { response in
                self.discussion = ""
                CoreDataManager.save()
            })
        }
    }
    
    // MARK: Upload Image to server
    func uploadServerIfNeed() {
        if (id == nil || id!.int32Value < 0) && !uploading {
            uploading = true
            MStyleServer.addPhoto(self, completion: { (response: DataResponse<Any>?) in
                if let response = response {
                    if case .success(let value) = response.result {
                        if let dictionary = value as? [String: AnyObject] {
                            self.updateUserInfo(dictionary)
                            self.renameLocalPhoto()
                            CoreDataManager.save()
                        }
                        self.postWallIfNeed()
                    }
                }
                self.uploading = false
            })
        }
    }
    
    // MARK: Local Data
    func saveTo(context: NSManagedObjectContext) {
        guard let entity = NSEntityDescription.entity(forEntityName: Photo.entityName, in: context) else {
            return
        }
        let localPhoto = Photo(entity: entity, insertInto: context)
        localPhoto.width = width
        localPhoto.height = height
        localPhoto.separator1 = separator1
        localPhoto.separator2 = separator2
        
        localPhoto.id = id
        localPhoto.localId = localId
        localPhoto.userId = userId
        localPhoto.contentType = contentType
        localPhoto.size = size
        localPhoto.typeString = typeString
        localPhoto.createdDate = createdDate
        localPhoto.likeUsers = likeUsers
        localPhoto.modeString = modeString
    }
    
    func saveImage(_ image: UIImage) {
        if let photoPath = photoPath, let data = UIImageJPEGRepresentation(image, 0.8) {
            do {
                try data.write(to: URL(fileURLWithPath: photoPath), options: .atomic)
            } catch {
                print("Error writing file: \(error)")
            }
        }
    }
    
    func renameLocalPhoto() {
        guard let localId = localId else {
            return
        }
        guard let userId = userId, let id = id else {
            return
        }
        let oldName = "Local-Photo-\(localId).jpg"
        let oldPath = (applicationDocumentsDirectory as NSString).appendingPathComponent(oldName)
        let newName = "Photo-\(userId)-\(id).jpg"
        let newPath = (applicationDocumentsDirectory as NSString).appendingPathComponent(newName)
        let fileManager = FileManager.default
        do {
            try fileManager.moveItem(atPath: oldPath, toPath: newPath)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error) when rename from : \(oldName) to : \(newName)")
        }
    }
    
    fileprivate var photoPath: String? {
        let filename: String = {
            if let userId = userId, let id = id {
                return "Photo-\(userId)-\(id).jpg"
            } else {
                return "Local-Photo-\(localId ?? 0).jpg"
            }
        }()
        return (applicationDocumentsDirectory as NSString).appendingPathComponent(filename)
    }
    
    var photoURL: URL? {
        guard let photoPath = photoPath else {
            return nil
        }
        
        return URL(fileURLWithPath: photoPath)
    }
    
    var isLastLocalPhoto: Bool {
        guard let localId = localId else {
            return false
        }
        return localId.intValue == UserDefaultsHelper.integerForKey(.localPhotoId) - 1
    }
    
    class func nextLocalPhotoID() -> Int {
        let currentID = UserDefaultsHelper.integerForKey(.localPhotoId)
        UserDefaultsHelper.setInteger(currentID + 1, forKey: .localPhotoId)
        UserDefaultsHelper.synchronize()
        return currentID
    }
}

// MARK: Equatable
func ==(lhs: Photo, rhs: Photo) -> Bool {
    return lhs.id != nil && lhs.id == rhs.id
}

// MARK: Delegate
protocol PhotoDelegate: class {
    func photoDidDownloadSuccessful(_ photo: Photo)
    func photoDidDownloadFail(_ photo: Photo, error: Error)
}

extension PhotoDelegate {
    func photoDidDownloadSuccessful(_ photo: Photo) {
    }
    func photoDidDownloadFail(_ photo: Photo, error: Error) {
    }
}
