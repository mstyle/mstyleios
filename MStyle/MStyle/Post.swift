//
//  Post.swift
//  MStyle
//
//  Created by luan on 10/3/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class Post {
    
    fileprivate struct Key {
        static let Discussion = "discussion"
        static let FbLink = "fbLink"
        static let Id = "id"
        static let Photo = "photo"
        static let PhotoId = "photoId"
        static let User = "user"
        static let UserId = "userId"
        static let postedDate = "postedDate"
    }
    
    var id: Int?
    
    var discussion: String?
    fileprivate var fbLink: String?
    var photo: Photo?
    var image: UIImage?
    var photoId: Int?
    fileprivate var userId: Int?
    var user: User?
    
    var photoHeight: Int?
    var photoWidth: Int?
    
    var postedDate: String?
    var postDate: Date?
    
    var request: Request?
    
    init(discussion: String, photoId: Int) {
        self.discussion = discussion
        self.photoId = photoId
    }
    
    init(dictionary: [String: AnyObject]) {
        discussion = dictionary[Key.Discussion] as? String
        fbLink = dictionary[Key.FbLink] as? String
        id = dictionary[Key.Id] as? Int
        postedDate = dictionary[Key.postedDate] as? String
        if let dateString = postedDate {
            postDate = dateFromString(dateString)
        }
        
        if let photoDic = dictionary[Key.Photo] as? [String: AnyObject] {
            //photo = Photo(dictionary: photoDic)
            photoWidth = photoDic[Photo.Key.kWidth] as? Int
            photoHeight = photoDic[Photo.Key.kHeight] as? Int
        }
        photoId = dictionary[Key.PhotoId] as? Int
        if let photoId = photoId {
            photo = CoreDataManager.getPhotoWithId(photoId)
        }
        
        if let userDic = dictionary[Key.User] as? [String: AnyObject] {
            user = User(dictionary: userDic)
        }
        userId = dictionary[Key.UserId] as? Int
    }
    
    deinit {
        request?.cancel()
        request = nil
    }
    
    // MARK: variables
    
    var dictionary: [String: AnyObject] {
        var dictionary: [String: AnyObject] = [:]
        
        if let discussion = self.discussion {
            dictionary[Key.Discussion] = discussion as AnyObject
        }
        if let photoId = self.photoId {
            dictionary[Key.PhotoId] = photoId as AnyObject
        }
        if let userId = self.userId {
            dictionary[Key.UserId] = userId as AnyObject
        }
        
        return dictionary
    }
    
    var date: String {
        get {
            guard let date = postDate else {
                return ""
            }
            
            return dentaStringFromDate(date)
        }
    }
    
    // MARK: Get Image
    func getImage(_ size: CGSize, completion: @escaping ((UIImage?) -> Void)) {
        let queue = DispatchQueue.global(qos: .userInitiated)
        queue.async {
            if let image = self.image {
                DispatchQueue.main.async {
                    completion(image)
                }
                return
            }
            
            
            if let image = self.photo?.image?.imageScaleFit(size * 1.5) {
                self.image = self.photo?.type == PhotoType.full ? image.cropByScale(0.8) : image
                DispatchQueue.main.async {
                    completion(self.image)
                }
            } else if let photoId = self.photoId {
                self.request = MStyleServer.downloadPhoto(photoId, size: size) {
                    [weak self] image in
                    self?.image = self?.photo?.type == PhotoType.full ? image?.cropByScale(0.8) : image
                    DispatchQueue.main.async() {
                        completion(self?.image)
                    }
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
    }
}
