//
//  MixVC
//  MStyle
//
//  Created by luan on 8/18/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit
import Alamofire

class MixVC: UIViewController {
    // MARK: Constraint
    fileprivate let kgoToShareSegue = "goToShare"
    fileprivate let kNotEnoughClosetSegue = "NotEnoughCloset"
    fileprivate let kPopupDeletePhotoSegue = "PopupDeletePhoto"
    fileprivate let kChooseSortTypeSegue = "chooseSortType"
    
    fileprivate let kMinimumClotherForMix = 3
    fileprivate var defaultOffset = CGPoint(x: 0, y: 0)
    
    fileprivate let selectedTitleColor = UIColor(netHex: 0x4A4A4A)
    fileprivate let normalTitleColor = UIColor(netHex: 0x9B9B9B)
    
    // MARK: Variable
    fileprivate var firstTime = false
    fileprivate var isCurrentUser: Bool {
        get {
            return user === User.currentUser
        }
    }
    var user: User!
    var photo: Photo! {
        didSet {
            if isViewLoaded && photo !== oldValue {
                updateMainPhoto()
            }
        }
    }
    var photos = [Photo]()
    var mixPhotos = [Photo]()
    
    fileprivate var mainViewScale: CGFloat = 1.0
    fileprivate var closetPhotoWidth: CGFloat {
        return view.bounds.width / 3
    }
    fileprivate var closetLongPressGes: UILongPressGestureRecognizer!
    
    fileprivate var isFirstMix = true
    fileprivate var isMixMode = false {
        didSet {
            mixHelpButton.isEnabled = !isMixMode
            targetScrollView.isHidden = isMixMode
            
            line1ImageView.isHidden = !isMixMode || isSkirtMode
            line2ImageView.isHidden = !isMixMode
            
            filterViewBottomConstraint.constant = isMixMode ? 0 : -34
            animateConstaint()
            
            if isMixMode {
                isFirstMix = true
                navigationBar.barTintColor = Color.kMixNavigationBackgroundColor
                view.backgroundColor = Color.kMixNavigationBackgroundColor
                navigationBar.pushItem(mixNavigationItem, animated: true)
                
                let index = mixPhotos.index(of: photo) ?? 0
                if isSkirtMode {
                    skirtScrollView.showWithFrame(skirtFrame)
                    skirtScrollView.initWithIndex(index)
                } else {
                    shirtScrollView.showWithFrame(shirtFrame)
                    shirtScrollView.initWithIndex(index)
                    
                    
                    pantScrollView.showWithFrame(pantFrame)
                    pantScrollView.initWithIndex(index)
                }
                shoesScrollView.showWithFrame(shoesFrame)
                shoesScrollView.initWithIndex(index)
            } else {
                navigationBar.barTintColor = Color.kNavigationBackgroundColor
                view.backgroundColor = Color.kNavigationBackgroundColor
                navigationBar.popItem(animated: true)
                
                targetScrollView.setContentOffset(defaultOffset, animated: true)
                setTargetImageWithPhoto(photo)
                
                shirtScrollView.isHidden = true
                pantScrollView.isHidden = true
                shoesScrollView.isHidden = true
                skirtScrollView.isHidden = true
            }
            
            closetCollectionView.reloadData()
        }
    }
    
    fileprivate var isSkirtMode = false {
        didSet {
            clothesView.isHidden = isSkirtMode
            skirtView.isHidden = !isSkirtMode
            
            if isSkirtMode {
                clothesTitleLabel.textColor = normalTitleColor
                skirtTitleLabel.textColor = selectedTitleColor
            } else {
                clothesTitleLabel.textColor = selectedTitleColor
                skirtTitleLabel.textColor = normalTitleColor
            }
            
            guard isMixMode else {
                return
            }
            let index = mixPhotos.index(of: photo) ?? 0
            if isSkirtMode {
                skirtScrollView.showWithFrame(skirtFrame)
                skirtScrollView.initWithIndex(index)
                
                shirtScrollView.isHidden = true
                pantScrollView.isHidden = true
            } else {
                skirtScrollView.isHidden = true
                
                shirtScrollView.showWithFrame(shirtFrame)
                shirtScrollView.initWithIndex(index)
                pantScrollView.showWithFrame(pantFrame)
                pantScrollView.initWithIndex(index)
            }
        }
    }
    
    fileprivate lazy var hightlightView: UIView = {
        let view = UIView()
        view.layer.borderWidth = 3
        view.layer.borderColor = UIColor(netHex: 0x32B3CD).cgColor
        view.isUserInteractionEnabled = false
        self.mainView.addSubview(view)
        return view
    }()
    
    fileprivate var allPhotoMix = true {
        didSet {
            allPhotoButton.isSelected = allPhotoMix
            mixPhotoButton.isSelected = !allPhotoMix
        }
    }
    
    fileprivate var sortType: SortType = .date {
        didSet {
            if oldValue == sortType {
                return
            }
            sortMixPhoto()
            closetCollectionView.performBatchUpdates({
                self.closetCollectionView.reloadSections(IndexSet(integer: 0))
            }, completion: nil)
        }
    }
    fileprivate var editMode: PartClother = .none {
        willSet {
            if newValue == editMode {
                return
            }
            isFirstMix = false
            
            switch editMode {
            case .shirt:
                shirtButton.show()
            case .pant:
                pantButton.show()
            case .shoes:
                shoesButton.show()
            case .skirt:
                skirtButton.show()
            default:
                break
            }
        }
        didSet {
            if oldValue == editMode {
                return
            }
            if !isMixMode && editMode != .none {
                mixTapped()
            }
            
            editView.isHidden = false
            updateCollectionCell()
            hightlightView.isHidden = false
            
            switch editMode {
            case .shirt:
                hightlightView.frame = shirtFrame
                
                shirtButton.hide()
                let position = mixView.convert(shirtButton.center, from: clothesView)
                var fromPosition = position
                fromPosition.x += mixView.frame.width
                editView.moveTo(position, fromPosition: fromPosition)
                
                nextButton.isHidden = !shirtScrollView.canNext
                prevButton.isHidden = !shirtScrollView.canPrev
            case .pant:
                hightlightView.frame = pantFrame
                
                pantButton.hide()
                let position = mixView.convert(pantButton.center, from: clothesView)
                var fromPosition = position
                fromPosition.x += mixView.frame.width
                editView.moveTo(position, fromPosition: fromPosition)
                
                nextButton.isHidden = !pantScrollView.canNext
                prevButton.isHidden = !pantScrollView.canPrev
            case .shoes:
                hightlightView.frame = shoesFrame
                
                shoesButton.hide()
                let position = mixView.convert(shoesButton.center, from: shoesView)
                var fromPosition = position
                fromPosition.x += mixView.frame.width
                editView.moveTo(position, fromPosition: fromPosition)
                
                nextButton.isHidden = !shoesScrollView.canNext
                prevButton.isHidden = !shoesScrollView.canPrev
            case .skirt:
                hightlightView.frame = skirtFrame
                
                skirtButton.hide()
                let position = mixView.center
                var fromPosition = position
                fromPosition.x += mixView.frame.width
                editView.moveTo(position, fromPosition: fromPosition)
                
                nextButton.isHidden = !skirtScrollView.canNext
                prevButton.isHidden = !skirtScrollView.canPrev
            case .none:
                hightlightView.isHidden = true
                
                let fromPosition = editView.center
                var position = fromPosition
                position.x += mixView.frame.width
                editView.moveTo(position, fromPosition: fromPosition, duration: 0.3, options: .curveEaseOut)
                
                nextButton.isHidden = true
                prevButton.isHidden = true
            }
        }
    }
    
    fileprivate var minPercent: CGFloat {
        return line1ImageView.valuePercent
    }
    fileprivate var maxPercent: CGFloat {
        return line2ImageView.valuePercent
    }
    
    // MARK: Variable for mix
    var targetImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    fileprivate var skirtFrame: CGRect {
        return CGRect(x: 0, y: 0, width: targetScrollView.bounds.width, height: line2ImageView.center.y)
    }
    fileprivate lazy var skirtScrollView: MixScrollView = {
        return self.createMixScrollView()
    }()
    
    fileprivate var shirtFrame: CGRect {
        return CGRect(x: 0, y: 0, width: targetScrollView.bounds.width, height: line1ImageView.center.y)
    }
    fileprivate lazy var shirtScrollView: MixScrollView = {
        return self.createMixScrollView()
    }()
    
    fileprivate var pantFrame: CGRect {
        let minYOffset = line1ImageView.center.y
        let maxYOffset = line2ImageView.center.y
        
        return CGRect(x: 0, y: minYOffset, width: targetScrollView.bounds.width, height: maxYOffset - minYOffset)
    }
    fileprivate lazy var pantScrollView: MixScrollView = {
        return self.createMixScrollView()
    }()
    
    fileprivate var shoesFrame: CGRect {
        let yOffset = line2ImageView.center.y
        return CGRect(x: 0, y: yOffset, width: targetScrollView.bounds.width, height: targetScrollView.bounds.height - yOffset)
    }
    fileprivate lazy var shoesScrollView: MixScrollView = {
        return self.createMixScrollView()
    }()
    
    // MARK: Outlet
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var targetScrollView: UIScrollView!
    @IBOutlet weak var closetCollectionView: UICollectionView!
    @IBOutlet weak var targetImageIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var mixView: UIView!
    @IBOutlet weak var clothesView: UIView!
    @IBOutlet weak var skirtView: UIView!
    @IBOutlet weak var editView: UIView!
    @IBOutlet weak var shoesView: UIView!
    
    @IBOutlet weak var mixHelpButton: UIButton!
    @IBOutlet weak var shirtButton: UIButton!
    @IBOutlet weak var pantButton: UIButton!
    @IBOutlet weak var shoesButton: UIButton!
    @IBOutlet weak var skirtButton: UIButton!
    
    @IBOutlet weak var line1ImageView: DraggableLineView!
    @IBOutlet weak var line2ImageView: DraggableLineView!
    
    @IBOutlet weak var closetCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var mixViewWidth: NSLayoutConstraint!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var prevButton: UIButton!
    
    @IBOutlet var navigationTitleView: UIView!
    @IBOutlet weak var clothesTitleLabel: UILabel!
    @IBOutlet weak var skirtTitleLabel: UILabel!
    
    @IBOutlet var mixNavigationTitleView: UIView!    
    
    @IBOutlet weak var listButton: UIButton!
    @IBOutlet weak var allPhotoButton: UIButton!
    @IBOutlet weak var mixPhotoButton: UIButton!
    @IBOutlet weak var filterViewBottomConstraint: NSLayoutConstraint!
    
    // MARK: Navigation
    weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var navigationBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var navigationBar: UINavigationBar!
    fileprivate lazy var fullNavigationItem: UINavigationItem = {
        let navigationItem = UINavigationItem()
        let backButton = UIBarButtonItem(image: UIImage(named: "cancel_icon"), style: .plain, target: self, action: #selector(cancelTapped))
        
        let nextButton = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(nextTapped))
        nextButton.isEnabled = false
        
        navigationItem.leftBarButtonItem = backButton
        navigationItem.title = "Your Closets"
        navigationItem.rightBarButtonItem = nextButton
        
        self.saveButton = nextButton
        return navigationItem
    }()
    fileprivate lazy var mixNavigationItem: UINavigationItem = {
        let navigationItem = UINavigationItem()
        let titleImageView = UIImageView(image: UIImage(named: "random-icon"))
        let backButton = UIBarButtonItem(image: UIImage(named: "cancel_icon"), style: .plain, target: self, action: #selector(cancelMixTapped))
        backButton.tintColor = UIColor.white
        
        let okButton = UIBarButtonItem(image: UIImage(named: "ok-icon"), style: .plain, target: self, action: #selector(saveMixTapped))
        okButton.tintColor = UIColor.white
        
        navigationItem.leftBarButtonItem = backButton
        navigationItem.titleView = titleImageView
        navigationItem.rightBarButtonItem = okButton
        
        return navigationItem
    }()
    
    // MARK:
    // MARK: Override ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showFirstPage = false
        
        line1ImageView.delegate = self
        line2ImageView.delegate = self
        line1ImageView.contentView = targetScrollView
        line2ImageView.contentView = targetScrollView
        
        targetScrollView.addSubview(targetImageView)
        
        listButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 4)
        listButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: -4, bottom: 0, right: 4)
        listButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 4, bottom: 0, right: -4)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
        if #available(iOS 11.0, *) {
            navigationBarHeightConstraint.constant = 44
        }
        fullNavigationItem.titleView = navigationTitleView
        mixNavigationItem.titleView = mixNavigationTitleView
        navigationBar.setItems([fullNavigationItem], animated: false)
        targetScrollView.contentOffset = defaultOffset
        
        if isCurrentUser {
            closetLongPressGes = UILongPressGestureRecognizer(target: self, action: #selector(closetLongPress(_:)))
            closetLongPressGes.delaysTouchesBegan = true
            closetCollectionView.addGestureRecognizer(closetLongPressGes)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        var position = editView.center
        position.x += mixView.frame.width
        editView.center = position
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        performDisappear()
    }
    
    func performDisappear() {
        if isCurrentUser {
            closetCollectionView.removeGestureRecognizer(closetLongPressGes)
        }
        for photo in photos {
            photo.cacheImage = nil
        }
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !firstTime {
            firstTime = true
            
            let layout = closetCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout.itemSize = CGSize(width: closetPhotoWidth, height: closetPhotoWidth)
            
            getAllPhoto()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segues.saveMix {
            if let saveMixVC = segue.destination as? SaveMixPhotoVC {
                saveMixVC.photo = sender as? Photo
            }
        } else if segue.identifier == kgoToShareSegue, let image = sender as? UIImage?, let shareVC = segue.destination as? ShareVC {
            shareVC.photo = photo
            shareVC.image = image
        } else if segue.identifier == kPopupDeletePhotoSegue,
            let indexPath = sender as? IndexPath,
            let cell = closetCollectionView.cellForItem(at: indexPath) as? ClosetCell,
            let popupDeleteVC = segue.destination as? PopupDeletePhotoVC {
            
            popupDeleteVC.image = cell.closetImageView.image
            popupDeleteVC.indexPath = indexPath
            popupDeleteVC.delegate = self
        } else if segue.identifier == kChooseSortTypeSegue, let chooseSortVC = segue.destination as? SortViewController {
            chooseSortVC.sortType = sortType
        }
    }
    
    // MARK:
    // MARK: Action
    @IBAction func nextMixPhoto() {
        switch editMode {
        case .shirt:
            shirtScrollView.nextMixPhoto()
            
            nextButton.isHidden = !shirtScrollView.canNext
            prevButton.isHidden = !shirtScrollView.canPrev
        case .skirt:
            skirtScrollView.nextMixPhoto()
            
            nextButton.isHidden = !skirtScrollView.canNext
            prevButton.isHidden = !skirtScrollView.canPrev
        case .pant:
            pantScrollView.nextMixPhoto()
            
            nextButton.isHidden = !pantScrollView.canNext
            prevButton.isHidden = !pantScrollView.canPrev
        case .shoes:
            shoesScrollView.nextMixPhoto()
            
            nextButton.isHidden = !shoesScrollView.canNext
            prevButton.isHidden = !shoesScrollView.canPrev
        default:
            break
        }
    }
    
    @IBAction func prevMixPhoto() {
        switch editMode {
        case .shirt:
            shirtScrollView.prevMixPhoto()
            
            nextButton.isHidden = !shirtScrollView.canNext
            prevButton.isHidden = !shirtScrollView.canPrev
        case .skirt:
            skirtScrollView.prevMixPhoto()
            
            nextButton.isHidden = !skirtScrollView.canNext
            prevButton.isHidden = !skirtScrollView.canPrev
        case .pant:
            pantScrollView.prevMixPhoto()
            
            nextButton.isHidden = !pantScrollView.canNext
            prevButton.isHidden = !pantScrollView.canPrev
        case .shoes:
            shoesScrollView.prevMixPhoto()
            
            nextButton.isHidden = !shoesScrollView.canNext
            prevButton.isHidden = !shoesScrollView.canPrev
        default:
            break
        }
    }
    
    func saveMixTapped() {
        editMode = .none
        if let image = getMixedImage() {
            line1ImageView.targetImageHeight = image.size.height
            line2ImageView.targetImageHeight = image.size.height
            
            if isCurrentUser {
                if let mixPhoto = CoreDataManager.createPhotoWithImage(image, separator1: line1ImageView.value, separator2: line2ImageView.value, type: .mixed) {
                    mixPhoto.uploadServerIfNeed()
                    mixPhoto.saveImage(image)
                    photo = mixPhoto
                    CoreDataManager.save()
                    user.photos.append(photo)
                    if let saveMixVC = storyboard?.instantiateViewController(withIdentifier: "SaveMixPhotoVC") as? SaveMixPhotoVC {
                        saveMixVC.photo = photo
                        if var viewControllers = navigationController?.viewControllers {
                            performDisappear()
                            viewControllers.removeLast()
                            viewControllers.append(saveMixVC)
                            navigationController?.setViewControllers(viewControllers, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    func cancelMixTapped() {
        isMixMode = false
        editMode = .none
    }
    
    private func mixTapped() {
        if targetImageView.image == nil {
            showHUDDownloading()
            return
        }
        
        if photos.count >= kMinimumClotherForMix {
            isMixMode = true
        } else {
            performSegue(withIdentifier: kNotEnoughClosetSegue, sender: nil)
        }
    }
    
    @IBAction func skirtModeChanged(_ sender: UISwitch) {
        isSkirtMode = sender.isOn
    }
    
    @IBAction func shoesTapped() {
        editMode = .shoes
    }
    
    @IBAction func pantTapped() {
        editMode = .pant
    }
    
    @IBAction func shirtTapped() {
        editMode = .shirt
    }
    
    @IBAction func skirtTapped() {
        editMode = .skirt
    }
    
    func cancelTapped() {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextTapped() {
        if let image = getFullImage() {
            goToShareVC(image)
        }
    }
    
    @IBAction func filterAllPhoto() {
        guard !allPhotoMix else {
            return
        }
        allPhotoMix = true
        mixPhotos = user.photos
        sortMixPhoto()
        closetCollectionView.performBatchUpdates({
            self.closetCollectionView.reloadSections(IndexSet(integer: 0))
        }, completion: nil)
    }
    
    @IBAction func filterMixedPhoto() {
        guard allPhotoMix else {
            return
        }
        allPhotoMix = false
        mixPhotos = user.photos.filter({ photo -> Bool in
            return photo.type == .mixed
        })
        sortMixPhoto()
        closetCollectionView.performBatchUpdates({
            self.closetCollectionView.reloadSections(IndexSet(integer: 0))
        }, completion: nil)
    }
    
    func sortMixPhoto() {
        let sortFunc: (Photo, Photo) -> Bool
        switch sortType {
        case .date:
            sortFunc = { (photo1, photo2) -> Bool in
                return photo1.createdDate > photo2.createdDate
            }
        case .love:
            sortFunc = { (photo1, photo2) -> Bool in
                return photo1.likeUsers.count > photo2.likeUsers.count
            }
        default:
            sortFunc = { (photo1, photo2) -> Bool in
                return arc4random_uniform(2) == 0
            }
        }
        mixPhotos = mixPhotos.sorted(by: sortFunc)
    }
    
    @IBAction func backFromSortType(segue: UIStoryboardSegue) {
        if let chooseSortTypeVC = segue.source as? SortViewController {
            sortType = chooseSortTypeVC.sortType
        }
    }
    
    @IBAction func listPhoto() {
        performSegue(withIdentifier: kChooseSortTypeSegue, sender: nil)
    }
    
    // MARK: Photo
    fileprivate func getAllPhoto() {
        photos = user.photos
        mixPhotos = photos
        sortMixPhoto()
        updateMainPhoto()
        closetCollectionView.reloadData()
    }
    
    fileprivate func addPhoto(_ photo: Photo, atIndex: Int) {
        if atIndex < photos.count {
            photos.insert(photo, at: atIndex)
        } else {
            photos.append(photo)
        }
    }
    
    fileprivate func photoWithId(_ id: Int) -> Photo? {
        for photo in photos where photo.id?.intValue == id {
            return photo
        }
        return nil
    }
    
    fileprivate func updatePhotos() {
        if photos.count > 0 {
            if photo == nil {
                selectClosetIndex(0)
                photo = photos[0]
            } else if let selectedItem = photos.index(of: photo) {
                updateMainPhoto()
                selectClosetIndex(selectedItem)
            }
        }
    }
    
    // MARK: UI
    fileprivate func selectClosetIndex(_ index: Int) {
        closetCollectionView.selectItem(at: IndexPath(item: index, section: 0), animated: true, scrollPosition: .left)
    }
    
    fileprivate func goToShareVC(_ image: UIImage?) {
        performSegue(withIdentifier: kgoToShareSegue, sender: image)
    }
    
    fileprivate func setTargetImageWithPhoto(_ photo: Photo?) {
        targetImageView.image = photo?.image?.imageScaleFit(targetImageView.frame.size * 2)
    }
    
    fileprivate func updateMainPhoto() {
        let navigationBarHeight = navigationBar.frame.height
        let mainHeight = view.frame.height
        let boundsSize = view.bounds.size
        let minimumClosetHeight = closetPhotoWidth * 1.06
        
        let queue = DispatchQueue.global(qos: .userInitiated)
        queue.async {
            guard let photo = self.photo else {
                self.targetImageView.image = nil
                return
            }
            
            let mainSize: CGSize = {
                let height = mainHeight - navigationBarHeight
                return CGSize(width: boundsSize.width,
                              height: height)
            }()
            let image = photo.image
            let imageSize: CGSize = {
                if let image = image {
                    return image.size
                } else {
                    return CGSize(width: CGFloat(photo.width), height: CGFloat(photo.height))
                }
            }()
            self.line1ImageView.targetImageHeight = imageSize.height
            self.line2ImageView.targetImageHeight = imageSize.height
            
            var mixWidth: CGFloat = 60
            var imageViewSize = CGSize(width: mainSize.width - mixWidth, height: 0)
            var scale = imageViewSize.width / imageSize.width
            imageViewSize.height = scale * imageSize.height
            var closetHeight = mainSize.height - imageViewSize.height
            
            if closetHeight < minimumClosetHeight {
                closetHeight = minimumClosetHeight
                imageViewSize.height = mainSize.height - closetHeight
                scale = imageViewSize.height / imageSize.height
                imageViewSize.width = scale * imageSize.width
                mixWidth = self.view.bounds.width - imageViewSize.width
            }
            self.mainViewScale = scale
            DispatchQueue.main.async {
                self.closetCollectionHeight.constant = closetHeight
                self.mixViewWidth.constant = mixWidth
                self.line1ImageView.value = photo.minSeparator
                self.line2ImageView.value = photo.maxSeparator
                
                if image != nil {
                    self.targetImageIndicator.stopAnimating()
                    self.targetScrollView.backgroundColor = .white
                } else {
                    self.targetImageIndicator.startAnimating()
                    MStyleServer.downloadPhoto(photo)
                    self.targetScrollView.backgroundColor = .black
                }
                
                self.animateConstaint()
                let scale : CGFloat = 1.0//1.25
                
                self.targetImageView.frame = CGRect(origin: .zero, size: imageViewSize * scale)
                self.targetScrollView.contentSize = imageViewSize
                self.defaultOffset = CGPoint(x: imageViewSize.width * (scale - 1.0) * 0.5,
                                             y: imageViewSize.height * (scale - 1.0) * 0.5)
                self.targetScrollView.contentOffset = self.defaultOffset
                self.targetImageView.image = image
            }
        }
    }
    
    fileprivate func updateCollectionCell() {
        for cell in closetCollectionView.visibleCells {
            if let closetCell = cell as? ClosetCell {
                closetCell.showPhotoWithPart(editMode)
            }
        }
    }
    
    fileprivate func animateConstaint() {
        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.4, initialSpringVelocity: 10.0, options: .curveEaseIn, animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    struct Segues {
        static let saveMix = "saveMix"
    }
}

// MARK:
// MARK: Collection Closet Datasource

extension MixVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return isMixMode ? mixPhotos.count : photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ClosetCell.kIdentifier, for: indexPath) as! ClosetCell
        let photo = isMixMode ? mixPhotos[indexPath.row] : photos[indexPath.row]
        photo.delegate = self
        cell.configuationWithPhoto(photo, editMode: editMode)
        return cell
    }
}

// MARK:
// MARK: Collection Delegate

extension MixVC: UICollectionViewDelegate {
    func closetLongPress(_ gestureRecognizer: UILongPressGestureRecognizer) {
        guard isCurrentUser else {
            return
        }
        if isMixMode {
            return
        }
        
        if gestureRecognizer.state != .ended {
            return
        }
        
        let p = gestureRecognizer.location(in: closetCollectionView)
        if let indexPath = closetCollectionView.indexPathForItem(at: p) {
            performSegue(withIdentifier: kPopupDeletePhotoSegue, sender: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedPhoto: Photo? = isMixMode ? mixPhotos[indexPath.row] : photos[indexPath.row]
        
        switch editMode {
        case .none:
            if selectedPhoto != nil {
                photo = selectedPhoto
            }
            shoesScrollView.initWithIndex(indexPath.row)
            if isSkirtMode {
                skirtScrollView.initWithIndex(indexPath.row)
            } else {
                shirtScrollView.initWithIndex(indexPath.row)
                pantScrollView.initWithIndex(indexPath.row)
            }
        case .shoes: shoesScrollView.initWithIndex(indexPath.row)
        case .pant: pantScrollView.initWithIndex(indexPath.row)
        case .shirt: shirtScrollView.initWithIndex(indexPath.row)
        case .skirt: skirtScrollView.initWithIndex(indexPath.row)
        }
    }
}

// MARK: Mix Photo
extension MixVC {
    fileprivate func getFullImage() -> UIImage? {
        return photo.image
    }
    
    fileprivate func getMixedImage() -> UIImage? {
        guard let image = photo.image else {
            return nil
        }
        let size = image.size
        
        if isSkirtMode {
            UIGraphicsBeginImageContextWithOptions(size, true, UIScreen.main.scale)
            UIGraphicsGetCurrentContext()!.saveGState()
            
            drawScrollView(skirtScrollView)
            drawScrollView(shoesScrollView, yOffset: size.height * maxPercent)
            
            drawLine(size.height * maxPercent, width: size.width)
            
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return newImage
        } else {
            UIGraphicsBeginImageContextWithOptions(size, true, UIScreen.main.scale)
            UIGraphicsGetCurrentContext()!.saveGState()
            
            drawScrollView(shirtScrollView)
            drawScrollView(pantScrollView, yOffset: size.height * minPercent)
            drawScrollView(shoesScrollView, yOffset: size.height * maxPercent)
            
            drawLine(size.height * minPercent, width: size.width)
            drawLine(size.height * maxPercent, width: size.width)
            
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return newImage
        }
    }
    
    fileprivate func drawScrollView(_ scrollView: MixScrollView, yOffset: CGFloat = 0.0) {
        
        let frame = scrollView.frame
        let sizeContent = scrollView.sizeContent
        let contentOffset = scrollView.contentOffset
        
        scrollView.sizeContent = sizeContent / mainViewScale
        scrollView.frame = frame / mainViewScale
        scrollView.contentOffset = contentOffset / mainViewScale
        
        let offset = scrollView.contentOffset
        let context = UIGraphicsGetCurrentContext()
        context!.translateBy(x: -offset.x, y: -offset.y + yOffset)
        scrollView.layer.render(in: UIGraphicsGetCurrentContext()!)
        context!.restoreGState()
        UIGraphicsGetCurrentContext()!.saveGState()
        
        scrollView.sizeContent = sizeContent
        scrollView.frame = frame
        scrollView.contentOffset = contentOffset
    }
    
    fileprivate func drawLine(_ yOffset: CGFloat, width: CGFloat) {
        let context = UIGraphicsGetCurrentContext()
        context!.setStrokeColor(UIColor.white.cgColor)
        context!.setLineWidth(1.0)
        context!.move(to: CGPoint(x: 0, y: yOffset))
        context!.addLine(to: CGPoint(x: width, y: yOffset))
        context!.strokePath()
        context!.restoreGState()
        UIGraphicsGetCurrentContext()!.saveGState()
    }
}

// MARK: PhotoDelegate

extension MixVC: PhotoDelegate {
    func photoDidDownloadSuccessful(_ photo: Photo) {
        if let index = photos.index(of: photo),
            let cell = closetCollectionView.cellForItem(at: IndexPath(row: index, section: 0)) as? ClosetCell {
            cell.configuationWithPhoto(photo, editMode: editMode)
        }
        
        if self.photo === photo {
            updateMainPhoto()
        }
    }
    
    func photoDidDownloadFail(_ photo: Photo, error: Error) {
        if let index = photos.index(of: photo) {
            photos.remove(at: index)
            let indexPath = IndexPath(row: index, section: 0)
            closetCollectionView.deleteItems(at: [indexPath])
        }
    }
}

// MARK: DragLine Delegate

extension MixVC: DragLineDelegate {
    func dragLineDidMove(_ dragLine: DraggableLineView) {
        if (!isMixMode) {
            return
        }
        
        if dragLine === line2ImageView {
            if isSkirtMode {
                skirtScrollView.frame = skirtFrame
            } else {
                pantScrollView.frame = pantFrame
            }
            shoesScrollView.frame = shoesFrame
            
            if editMode == .shoes {
                hightlightView.frame = shoesFrame
            } else if editMode == .pant {
                hightlightView.frame = pantFrame
            } else if editMode == .skirt {
                hightlightView.frame = skirtFrame
            }
        } else if dragLine === line1ImageView {
            shirtScrollView.frame = shirtFrame
            pantScrollView.frame = pantFrame
            if editMode == .shirt {
                hightlightView.frame = shirtFrame                
            } else if editMode == .pant {
                hightlightView.frame = pantFrame
            }
        }
    }
}

// MARK: MixScrollViewDelegate
extension MixVC: MixScrollViewDelegate {
    func didTappedMixScrollView(_ mixScrollView: MixScrollView) {
        if !isMixMode {
            return
        }
        
        if mixScrollView === skirtScrollView {
            editMode = .skirt
        } else if mixScrollView === shirtScrollView {
            editMode = .shirt
        } else if mixScrollView === pantScrollView {
            editMode = .pant
        } else if mixScrollView === shoesScrollView {
            editMode = .shoes
        }
    }
    
    func didMixScrollViewScrollIndex(_ mixScrollView: MixScrollView, index: Int) {
        if !isMixMode {
            return
        }
        
        if mixScrollView === skirtScrollView {
            editMode = .skirt
        } else if mixScrollView === shirtScrollView {
            editMode = .shirt
        } else if mixScrollView === pantScrollView {
            editMode = .pant
        } else if mixScrollView === shoesScrollView {
            editMode = .shoes
        }
        
        
        let indexPath = IndexPath(item: index, section: 0)
        closetCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .left)
    }
}

// MARK: Helper

extension MixVC {
    func createMixScrollView() -> MixScrollView {
        let scrollView = MixScrollView()
        scrollView.isHidden = true
        scrollView.photos = mixPhotos
        scrollView.sizeContent = targetScrollView.bounds.size
        scrollView.mixDelegate = self
        
        mainView.insertSubview(scrollView, aboveSubview: targetScrollView)
        return scrollView
    }
}

// MARK: PopupDeleteDelegate
extension MixVC: PopupDeleteDelegate {
    func deletePhoto(at indexPath: IndexPath) {
        var photos = isMixMode ? self.mixPhotos : self.photos
        guard indexPath.item < photos.count else {
            return
        }
        
        showHUDLoading()
        
        let photo = photos[indexPath.item]
        MStyleServer.deletePhoto(photo) { [weak self] success in
            if success {
                showHUDDeleted()
                
                guard let sSelf = self else {
                    return
                }
                guard !sSelf.isMixMode else {
                    return
                }
                CoreDataManager.delete(photo)
                
                sSelf.photos.remove(at: indexPath.item)
                if let fullIndex = sSelf.mixPhotos.index(of: photo) {
                    sSelf.mixPhotos.remove(at: fullIndex)
                }
                sSelf.closetCollectionView.reloadData()
                if sSelf.photo === photo {
                    if sSelf.photos.count == 0 {
                        sSelf.photo = nil
                    } else if indexPath.item < sSelf.photos.count {
                        sSelf.photo = sSelf.photos[indexPath.item]
                        sSelf.selectClosetIndex(indexPath.item)
                    } else {
                        sSelf.photo = sSelf.photos[0]
                        sSelf.selectClosetIndex(0)
                    }
                }
            } else {
                showHUDError()
            }
        }
    }
}
