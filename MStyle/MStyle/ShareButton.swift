//
//  ShareButton.swift
//  MStyle
//
//  Created by luan on 9/25/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit

@IBDesignable
class ShareButton: VerticalButton {

    @IBInspectable var radius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = radius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var pressColor: UIColor?
    fileprivate var normalColor: UIColor?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        normalColor = backgroundColor
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        UIView.animate(withDuration: 0.12, delay: 0.0, options: .curveEaseIn, animations: {
            self.backgroundColor = self.pressColor
            }, completion: nil)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        
        UIView.animate(withDuration: 0.12, delay: 0.0, options: .curveEaseIn, animations: {
            self.backgroundColor = self.normalColor
            }, completion: nil)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
        UIView.animate(withDuration: 0.12, delay: 0.0, options: .curveEaseIn, animations: {
            self.backgroundColor = self.normalColor
            }, completion: nil)
    }
}
