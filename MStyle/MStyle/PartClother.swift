//
//  Style.swift
//  MStyle
//
//  Created by Phu Quang Nguyen on 5/17/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import Foundation
import UIKit

enum PartClother {
    case none
    case shirt
    case pant
    case shoes
    case skirt
}
