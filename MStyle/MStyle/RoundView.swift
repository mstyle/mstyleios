//
//  RoundView.swift
//  MStyle
//
//  Created by luan on 8/28/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit

@IBDesignable
class RoundView: UIView {

    @IBInspectable var roundRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = roundRadius
        }
    }
}
