//
//  PhotoVC.swift
//  MStyle
//
//  Created by Minh Luan Tran on 5/29/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import UIKit
import Alamofire

class PhotoVC: UIViewController {
    
    var photo: Photo!
    var user: User!
    var request: DataRequest?
    
    // MARK: Outlet
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var mixButton: UIButton!
    
    // MARK: Override ViewController
    override func viewDidLoad() {
        super.viewDidLoad()

        photoImageView.image = photo.image
        mixButton.layer.cornerRadius = 21
        
        updateLikeUI()
        setupView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == kMixPhotoSegue {
            if let mixVC = segue.destination as? MixVC {
                mixVC.user = user
                mixVC.photo = photo
                mixVC.hidesBottomBarWhenPushed = true
            }
        }
    }

    
    fileprivate func updateLikeUI() {
        if user === User.currentUser {
            likeButton.isSelected = photo.favorited
        } else {
            likeButton.isSelected = photo.likeUsers.contains(User.currentUser.id)
        }
    }
    
    fileprivate func setupView() {
        if user === User.currentUser {
            facebookButton.isHidden = false
            mixButton.isHidden = false
            likeButton.isHidden = false
        } else {
            facebookButton.isHidden = true
            mixButton.isHidden = true
            likeButton.isHidden = true
        }
        if photo.type == .mixed {
            mixButton.isHidden = true
        }
    }
    
    // MARK: Action
    @IBAction func back() {
        if navigationController?.popViewController(animated: true) == nil {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func pressShareFacebook() {
        shareFacebook(photo.image, fromViewController: self)
    }
    
    @IBAction func like() {
        if user === User.currentUser {
            photo.favorited = !photo.favorited
            CoreDataManager.save()
            updateLikeUI()
        } else {
            let curUserId = User.currentUser.id
            if photo.likeUsers.contains(curUserId) {
                MStyleServer.unlikePhoto(photo) { [weak photo, weak self] success in
                    if let index = photo?.likeUsers.index(of: curUserId) {
                        photo?.likeUsers.remove(at: index)
                    }
                    self?.updateLikeUI()
                }
            } else {
                MStyleServer.likePhoto(photo) { [weak photo, weak self] success in
                    photo?.likeUsers.insert(curUserId)
                    self?.updateLikeUI()
                }
            }
        }
    }
    
    fileprivate func performDelete() {
        showHUDLoading()
        MStyleServer.deletePhoto(photo) { [weak photo, weak self, weak user] success in
            guard success else {
                showHUDFail()
                return
            }
            if self != nil {
                showHUDSuccess(completion: { [weak self] _ in
                    self?.navigationController?.popViewController(animated: true)
                })
            }
            guard let photo = photo else {
                return
            }
            guard let user = user else {
                return
            }
            if let index = user.photos.index(of: photo) {
                user.photos.remove(at: index)
            }
        }
    }
    
    @IBAction func delete() {
        let alertVC = UIAlertController(title: nil, message: "Do you want to delete this photo?", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { _ in
            self.performDelete()
        }
        alertVC.addAction(cancelAction)
        alertVC.addAction(deleteAction)
        
        present(alertVC, animated: true, completion: nil)
    }
    
    @IBAction func switchMode(_ sender: UISwitch) {
        photo.mode = sender.isOn ? .only : .friend
        CoreDataManager.save()
        if let request = request {
            request.cancel()
        }
        request = MStyleServer.updatePhotoMode(photo) { [weak self] success in
            guard let sSelf = self else {
                return
            }
            sSelf.request = nil
        }
    }
    
    @IBAction func saveToClothes() {
        showHUDLoading()
        MStyleServer.savePhoto(photo) { (success) in
            if success {
                showHUDSuccess()
            } else {
                showHUDFail()
            }
        }
    }
    
    // MARK: Constaint
    private let kMixPhotoSegue = "mixPhotoSegue"
}
