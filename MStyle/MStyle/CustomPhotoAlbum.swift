//
//  CustomPhotoAlbum.swift
//  MStyle
//
//  Created by luan on 10/25/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import Photos

class CustomPhotoAlbum {
    
    static let albumName = "Mupxi"
    static let sharedInstance = CustomPhotoAlbum()
    
    var assetCollection: PHAssetCollection!
    var image: UIImage?
    
    init() {
        
        func fetchAssetCollectionForAlbum() -> PHAssetCollection! {
            
            let fetchOptions = PHFetchOptions()
            fetchOptions.predicate = NSPredicate(format: "title = %@", CustomPhotoAlbum.albumName)
            let collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
            
            return collection.firstObject
        }
        
        if let assetCollection = fetchAssetCollectionForAlbum() {
            self.assetCollection = assetCollection
            return
        }
        
        PHPhotoLibrary.shared().performChanges({
            PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: CustomPhotoAlbum.albumName)
        }) { success, _ in
            if success {
                self.assetCollection = fetchAssetCollectionForAlbum()
                if let image = self.image {
                    self.saveImage(image)
                }
            }
        }
    }
    
    func saveImage(_ image: UIImage) {
        
        if assetCollection == nil {
            self.image = image
            return   // If there was an error upstream, skip the save.
        } else {
            self.image = nil
        }
        
        PHPhotoLibrary.shared().performChanges({
            let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
            let assetPlaceholder = assetChangeRequest.placeholderForCreatedAsset
            let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
            let fastEnumeration = NSArray(array: [assetPlaceholder!] as [PHObjectPlaceholder])
            albumChangeRequest!.addAssets(fastEnumeration)
        }, completionHandler: nil)
    }
}
