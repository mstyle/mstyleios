//
//  PostVC.swift
//  MStyle
//
//  Created by luan on 10/7/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit

class PostVC: UIViewController {
    
    // MARK: Outlet
    @IBOutlet weak var postTableView: UITableView!
    @IBOutlet var headerView: UIView!
    
    // MARK: Variables
    fileprivate var posts = [Post]()
    
    // MARK: Override ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //postTableView.estimatedRowHeight = 50
        //postTableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func didReceiveMemoryWarning() {
        var index = 0
        for post in posts {
            let indexPath = IndexPath(row: index, section: 0)
            index += 1
            
            if isVisible {
                if let cell = postTableView.cellForRow(at: indexPath) as? PostTableViewCell, cell.post === post {
                    continue
                } else if post.photo != nil {
                    continue
                }
            }
            post.image = nil
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        showHUDLoading()
        MStyleServer.getPosts(0, size: 20) { [weak self] (posts) in
            if self == nil {
                return
            }
            
            for post in posts {
                for (index, item) in self!.posts.enumerated() where post.id == item.id {
                    post.image = item.image
                    self!.posts.remove(at: index)
                    break
                }
            }
            self?.posts = posts
            self?.postTableView.reloadData()
            hideHUD()
        }
    }
    
    // MARK: Add Post
    fileprivate func imageForPost(_ post: Post) -> UIImage? {
        for item in posts where item.id == post.id {
            return item.image
        }
        return nil
    }
}

// MARK: TableView Datasource
extension PostVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell", for: indexPath) as! PostTableViewCell
        cell.post = posts[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let post = posts[indexPath.row]
        var heightImage: CGFloat = 0
        if let photo = post.photo {
            let ratio = CGFloat(photo.width) / CGFloat(photo.height)
            heightImage = (view.bounds.width - 16) / ratio
        } else if let width = post.photoWidth, let height = post.photoHeight {
            let ratio = CGFloat(width) / CGFloat(height)
            heightImage = (view.bounds.width - 16) / ratio
        }
        
        let widthDiscussion = view.frame.width - 88 - 16
        let discussion = post.discussion ?? ""
        let heightBottom = discussion.heightWithConstrainedWidth(widthDiscussion, font: .systemFont(ofSize: 14)) + 96
        
        //                 + heightInfo + bot margin
        return heightImage + 76 + 4 + heightBottom
    }
}

// MARK: TableView Delegate
extension PostVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
}
