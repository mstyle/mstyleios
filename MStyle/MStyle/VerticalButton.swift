//
//  VerticalButton.swift
//  MStyle
//
//  Created by luan on 8/28/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit

@IBDesignable
class VerticalButton: UIButton {
    
    @IBInspectable var isTextAbove: Bool = false

    override func layoutSubviews() {
        super.layoutSubviews()
        
        let kTextTopPadding:CGFloat = 3.0
        var titleLabelFrame = titleLabel!.frame
        titleLabel!.numberOfLines = 0
        
        let labelSize = titleLabel!.sizeThatFits(CGSize(width: self.contentRect(forBounds: self.bounds).width, height: CGFloat.greatestFiniteMagnitude))
        var imageFrame = self.imageView!.frame
        
        let fitBoxSize = CGSize(width: max(imageFrame.size.width, labelSize.width), height: labelSize.height + kTextTopPadding + imageFrame.size.height)
        let fitBoxRect = bounds.insetBy(dx: (bounds.size.width - fitBoxSize.width)/2, dy: (bounds.size.height - fitBoxSize.height)/2)
        
        if isTextAbove {
            imageFrame.origin.y = fitBoxRect.origin.y + labelSize.height + kTextTopPadding
            titleLabelFrame.origin.y = fitBoxRect.origin.y
        } else {
            imageFrame.origin.y = fitBoxRect.origin.y
            titleLabelFrame.origin.y = fitBoxRect.origin.y + imageFrame.size.height + kTextTopPadding
        }
        imageFrame.origin.x = fitBoxRect.midX - (imageFrame.size.width/2);
        imageView!.frame = imageFrame
        
        titleLabelFrame.size.width = labelSize.width
        titleLabelFrame.size.height = labelSize.height
        titleLabelFrame.origin.x = (self.frame.size.width / 2) - (labelSize.width / 2)
        titleLabel!.frame = titleLabelFrame
        titleLabel!.textAlignment = .center
        titleLabel!.numberOfLines = 0
    }
}
