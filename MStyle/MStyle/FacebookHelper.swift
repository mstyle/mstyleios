//
//  FacebookHelper.swift
//  MStyle
//
//  Created by luan on 9/2/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import Foundation
import UIKit
import FBSDKShareKit
import FBSDKLoginKit

func shareFacebook(_ image: UIImage?, text: String = "", fromViewController: UIViewController, delegate: FBSDKSharingDelegate? = nil) {
    let photo = FBSDKSharePhoto(image: image, userGenerated: true)
    let content = FBSDKSharePhotoContent()
    photo?.caption = text
    content.photos = [photo as Any]
    
    let dialog = FBSDKShareDialog()
    dialog.fromViewController = fromViewController
    dialog.shareContent = content
    dialog.delegate = delegate
    dialog.mode = .native
    
    if dialog.canShow() {
        dialog.show()
    } else {
        if FBSDKAccessToken.current() != nil && FBSDKAccessToken.current().hasGranted(FBConstaint.publishActionPermission) {
        } else {
            let login = FBSDKLoginManager()
            login.logIn(withPublishPermissions: [FBConstaint.publishActionPermission], from: fromViewController) { [weak fromViewController] (result, error) in
                if error != nil {
                    print("Progress error")
                } else if (result?.isCancelled)! {
                    print("Cancelled")
                } else if let fromViewController = fromViewController {
                    shareFacebook(image, text: text, fromViewController: fromViewController, delegate: fromViewController as? FBSDKSharingDelegate)
                }
            }
        }
    }
}
