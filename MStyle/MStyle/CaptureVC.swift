//
//  CaptureVC.swift
//  MStyle
//
//  Created by Phu Quang Nguyen on 5/15/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit
import AVFoundation
import PKHUD

enum Status: Int {
    case preview, still, error
}

class CaptureVC: UIViewController {
    
    struct Constant {
        static let CAPTURE_BUTTON_WIDTH: CGFloat = 60.0
        static let MAX_TIMER_CAMERA: Int = 5
    }
    
    @IBOutlet weak var cameraPreview: UIView!
    @IBOutlet weak var styleScrollView: UIScrollView!
    @IBOutlet weak var styleScrollViewWidth: NSLayoutConstraint!
    
    @IBOutlet weak var btnRightArrow: UIButton!
    @IBOutlet weak var btnLeftArrow: UIButton!
    @IBOutlet weak var imvCurrentStyle: UIImageView!
    @IBOutlet weak var btnFlashToggle: UIButton!
    @IBOutlet weak var timeButton: UIButton!
    @IBOutlet weak var detailTimeLabel: UILabel!
    @IBOutlet weak var btnCapture: CaptureButton!
    
    fileprivate var preview: AVCaptureVideoPreviewLayer?
    fileprivate var camera: XMCCamera?
    fileprivate var status: Status = .preview
    fileprivate lazy var userDefaults = UserDefaults.standard
    
    fileprivate var timer: Timer?
    fileprivate var countDownTime = 0
    
    fileprivate var stylesImage = [
        UIImage(named: "model-1"),
        UIImage(named: "model-2"),
        UIImage(named: "model-3")
    ]
    
    fileprivate let styleMarginX: CGFloat = 28
    fileprivate let styleMarginY: CGFloat = 23
    fileprivate var currentPage: Int = 0 {
        didSet {
            guard let targetImageView = targetImageView else {
                return
            }
            styleScrollViewWidth.constant = targetImageView.frame.width + styleMarginX
            var contentOffset = styleScrollView.contentOffset
            contentOffset.x = targetImageView.frame.origin.x - styleMarginX / 2
            styleScrollView.setContentOffset(contentOffset, animated: true)
            updateNavigationButtons()
        }
    }
    fileprivate var targetImageView: UIImageView? {
        get {
            guard currentPage >= 0 && currentPage < styleScrollView.subviews.count else {
                return nil
            }
            return styleScrollView.subviews[currentPage] as? UIImageView
        }
    }
    fileprivate var visibleRect: CGRect {
        get {
            guard let targetImageView = targetImageView else {
                return CGRect(origin: .zero, size: styleScrollView.frame.size)
            }
            return targetImageView.frame
        }
    }
    fileprivate var isStylesViewAnimating: Bool = false
    fileprivate var doesStylesViewOpen: Bool = false {
        didSet {
            imvCurrentStyle.isHidden = doesStylesViewOpen
        }
    }
    fileprivate var isStyleVisibile: Bool = true
    
    fileprivate var isTimerMode: Bool = false {
        didSet {
            timeButton.isSelected = isTimerMode
            UIView.animate(withDuration: 0.3, animations: {
                self.detailTimeLabel.alpha = self.isTimerMode ? 1 : 0
            }) 
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addMovableCaptureButton()
        
        isStyleVisibile = false
        
        let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        if !(device?.hasTorch)! {
            btnFlashToggle.isHidden = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        initializeCamera()
        establishVideoPreviewArea()
        
        addStyleViews()        
        currentPage = UserDefaultsHelper.integerForKey(.styleID)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.cameraPreview.layer.sublayers?.removeAll()
        
        for imageView in styleScrollView.subviews {
            imageView.removeFromSuperview()
        }
        
        userDefaults.setValue(NSNumber(value: Float(btnCapture.lastLocation.x)), forKey: Key.CaptureButtonPositionX)
        userDefaults.setValue(NSNumber(value: Float(btnCapture.lastLocation.y)), forKey: Key.CaptureButtonPositionY)
        
        super.viewWillDisappear(animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == Segues.savePhoto {
            if let savePhotoVC = segue.destination as? SavePhotoVC {
                savePhotoVC.image = sender as? UIImage
            }
        }
    }
    
    // MARK: - Action
    
    @IBAction func previousStyle(_ sender: UIButton) {
        currentPage -= 1
    }
    
    @IBAction func nextStyle(_ sender: UIButton) {
        currentPage += 1
    }
    
    func takePhoto() {
        
        if isTimerMode {
            timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateTimerCamera), userInfo: nil, repeats: false)
            countDownTime = Constant.MAX_TIMER_CAMERA
        } else {
            capturePhoto()
        }
    }
    
    @IBAction func toggleFlash(_ sender: UIButton) {
        
        let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        if (device?.hasTorch)! {
            do {
                try device?.lockForConfiguration()
                if (device?.torchMode == AVCaptureTorchMode.on) {
                    device?.torchMode = AVCaptureTorchMode.off
                    btnFlashToggle.isSelected = false
                } else {
                    try device?.setTorchModeOnWithLevel(1.0)
                    btnFlashToggle.isSelected = true
                }
                device?.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
    }
    
    @IBAction func toggleTime(_ sender: UIButton) {
        isTimerMode = !isTimerMode
    }
    
    @IBAction func dismiss(_ sender: UIBarButtonItem) {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Camera
    func capturePhoto() {
        timer?.invalidate()
        camera?.captureStillImage({ (image) -> Void in
            if let image = image {
                print("captured image")
                self.goToTagsAndPartsWithCapturedImage(image)
            } else {
                AlertHelper.showAlertWithTitle("MStyle", message: "Uh oh! Something went wrong. Try it again.", target: self, completion: nil)
            }
        })
    }
    
    func updateTimerCamera() {
        HUD.flash(.label("\(countDownTime)"))
        countDownTime -= 1
        if countDownTime == 0 {
            HUD.hide(afterDelay: 1, completion: nil)
            timer?.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(capturePhoto), userInfo: nil, repeats: false)
        } else {
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimerCamera), userInfo: nil, repeats: false)
        }

    }
    
    // MARK: - Init view, customize UI, delegate
    fileprivate func initializeCamera() {
        camera = XMCCamera(sender: self)
    }
    
    fileprivate func establishVideoPreviewArea() {
        preview = AVCaptureVideoPreviewLayer(session: self.camera?.session)
        preview?.videoGravity = AVLayerVideoGravityResizeAspectFill
        preview?.frame = self.cameraPreview.bounds
        cameraPreview.layer.addSublayer(self.preview!)
    }
    
    fileprivate func addMovableCaptureButton() {
        btnCapture.lastLocation = {
            if let positionX = userDefaults.value(forKey: Key.CaptureButtonPositionX) as? NSNumber,
                let positionY = userDefaults.value(forKey: Key.CaptureButtonPositionY) as? NSNumber {
                return CGPoint(x: CGFloat(positionX.floatValue),
                               y: CGFloat(positionY.floatValue))
            } else {
                return CGPoint(x: view.bounds.width / 2, y: view.bounds.height * 0.6)
            }
        }()
        btnCapture.onPress = { [weak self] in
            self?.takePhoto()
        }
    }
    
    fileprivate func addStyleViews() {
        
        var originX = styleMarginX / 2
        let originY = styleMarginY
        let height = cameraPreview.frame.height - styleMarginY * 2
        
        for image in stylesImage {
            guard let image = image else {
                continue
            }
            let imageView = UIImageView(image: image)
            let width = image.size.width * height / image.size.height
            
            imageView.frame = CGRect(x: originX, y: originY, width: width, height: height)
            imageView.contentMode = .scaleAspectFit
            styleScrollView.addSubview(imageView)
            originX += width + styleMarginX
        }
        styleScrollView.contentSize = CGSize(width: originX - styleMarginX / 2,
                                             height: self.cameraPreview.frame.height)
    }
    
    // MARK: Animate View, Update UI
    fileprivate func updateNavigationButtons() {
        let lastItemIndex = stylesImage.count - 1
        
        if currentPage == 0 {
            self.btnLeftArrow.isEnabled = false
            if currentPage == lastItemIndex {
                self.btnLeftArrow.isEnabled = true
            } else {
                self.btnRightArrow.isEnabled = true
            }
        } else if currentPage == lastItemIndex {
            self.btnLeftArrow.isEnabled = true
            self.btnRightArrow.isEnabled = false
        } else {
            self.btnLeftArrow.isEnabled = true
            self.btnRightArrow.isEnabled = true
        }
        
        UserDefaultsHelper.setInteger(currentPage, forKey: .styleID)
    }
    
    fileprivate func goToTagsAndPartsWithCapturedImage(_ image: UIImage) {
        performSegue(withIdentifier: Segues.savePhoto, sender: image)
    }

    struct Segues {
        static let savePhoto = "savePhoto"
    }
}

// MARK: Camera Delegate

extension CaptureVC: XMCCameraDelegate {
    
    func cameraSessionConfigurationDidComplete() {
        self.camera?.startCamera()
    }
    
    func cameraSessionDidBegin() {
        /*self.cameraStatus.text = ""
        UIView.animateWithDuration(0.225, animations: { () -> Void in
            self.cameraStatus.alpha = 0.0
            self.cameraPreview.alpha = 1.0
            self.cameraCapture.alpha = 1.0
            self.cameraCaptureShadow.alpha = 0.4;
        })*/
    }
    
    func cameraSessionDidStop() {
        /*self.cameraStatus.text = "Camera Stopped"
        UIView.animateWithDuration(0.225, animations: { () -> Void in
            self.cameraStatus.alpha = 1.0
            self.cameraPreview.alpha = 0.0
        })*/
    }
    
}

// MARK: ScrollView Delegate

extension CaptureVC: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        
        let pageWidth: CGFloat = scrollView.frame.width
        currentPage = Int(floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1)
    }
    
}
