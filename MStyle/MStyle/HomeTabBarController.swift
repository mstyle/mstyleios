//
//  HomeTabBarController.swift
//  MStyle
//
//  Created by Phu Quang Nguyen on 5/13/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit

class HomeTabBarController: UITabBarController {
    fileprivate let selectedIndicatorColor = UIColor(red: 213/255.0, green: 115/255.0, blue: 125/255.0, alpha: 1.0)
    var selectedIndicatorImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        User.getCurrentUserData()
        self.delegate = self
        
        let height = tabBar.frame.size.height
        let width = tabBar.frame.size.width / CGFloat(tabBar.items!.count)
        
        selectedIndicatorImage = UIImage().imageWithColor(selectedIndicatorColor, size: CGSize(width: width, height: height))
        showFirstPage = false
        if showFirstPage {
            selectedIndex = 2
            UITabBar.appearance().selectionIndicatorImage = nil
        } else {
            UITabBar.appearance().selectionIndicatorImage = selectedIndicatorImage
        }
    }
}

// MARK: Tab Bar Delegate

extension HomeTabBarController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if showFirstPage {
            return false
        }
        
        if viewController is SecondTabViewController {
            return false           
        } else {
            tabBar.selectionIndicatorImage = selectedIndicatorImage
        }
        return true
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.tag == 2 {
            if let captureVC = self.storyboard?.instantiateViewController(withIdentifier: "CaptureVC") as? CaptureVC {
                let navigationController = UINavigationController(rootViewController: captureVC)
                present(navigationController, animated: true, completion: nil)
            }
        } else if item.tag == 1 {
            if let navVC = viewControllers?[1] as? UINavigationController,
                let closetVC = navVC.viewControllers.first as? ClosetVC
            {
                closetVC.user = User.currentUser
            }
        }
    }
}
