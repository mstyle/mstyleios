//
//  LoadingViewController.swift
//  MStyle
//
//  Created by luan on 8/9/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit
import Alamofire
import FBSDKLoginKit

class LoadingViewController: UIViewController {
    
    fileprivate let goLoginSegueIdentifier = "GoLogin"
    fileprivate let goMainSegueIdentifier = "GoMain"
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var loadingView: UIView!
    
    @IBOutlet weak var tryAgainButton: UIButton!
    
    // MARK: Override ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tryAgainButton.alpha = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !MStyleServer.hadAccessToken() {
            if let _ = FBSDKAccessToken.current() {
                loginWithFacebook()
            } else {
                goToLoginVC()
            }
        } else {
            checkAuthenticated()
        }
    }
    
    // MARK: Action
    @IBAction func tryAgainTapped(_ sender: AnyObject) {
        showLoadingView()
        checkAuthenticated()
    }
    
    @IBAction func backToLogin(segue: UIStoryboardSegue) {
        
    }
    
    // MARK: animate view
    
    fileprivate func showTryAgainView() {
        activityIndicatorView.stopAnimating()
        UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions(), animations: {
            self.loadingView.alpha = 0
            self.tryAgainButton.alpha = 1
            }, completion: nil)
    }
    
    fileprivate func showLoadingView() {
        activityIndicatorView.startAnimating()
        UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions(), animations: {
            self.loadingView.alpha = 1
            self.tryAgainButton.alpha = 0
            }, completion: nil)
    }
    
    fileprivate func showDomainError() {
        let title = NSLocalizedString("Oops", comment: "Title when url domain wrong")
        let message = NSLocalizedString("Something go wrong. Please try again later", comment: "Message when url domain wrong")
        
        AlertHelper.showAlertWithTitle(title, message: message, target: self) { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
        showTryAgainView()
    }
    
    // MARK: Authenticate
    
    fileprivate func checkAuthenticated() {
        MStyleServer.isAuthenticated { [weak self] (response) in
            guard let sSelf = self else {
                return
            }
            
            if case Result.success = response.result {
                sSelf.goToMainVC()
            } else if case Result.failure(let error) = response.result {
                if response.response?.statusCode == 401 {
                    sSelf.getNewAccessToken()
                } else if (error as NSError).domain == NSURLErrorDomain {
                    sSelf.showDomainError()
                }
                else {
                    sSelf.getNewFBToken()
                }
                debugPrint("Error check Authenticated: \(error)")
            }
        }
    }
    
    fileprivate func getNewFBToken() {
        if FBSDKAccessToken.current() != nil && !FBSDKAccessToken.current().tokenString.isEmpty {
            loginWithFacebook()
        } else {
            let login = FBSDKLoginManager()
            login.logIn(withReadPermissions: FBConstaint.permission, from: self) { [weak self] (result, error) in
                if error != nil {
                    print("Progress error")
                } else if (result?.isCancelled)! {
                    print("Cancelled")
                } else {
                    self?.loginWithFacebook()
                }
            }
        }
    }
    
    fileprivate func getNewAccessToken() {
        MStyleServer.getNewAccessToken { [weak self] (response) in
            guard let sSelf = self else {
                return
            }
            
            guard let response = response else {
                sSelf.goToLoginVC()
                return
            }
            
            if case Result.success = response.result {
                sSelf.goToMainVC()
            } else if case Result.failure(let error) = response.result {
                if (error as NSError).domain == NSURLErrorDomain {
                    sSelf.showDomainError()
                } else if FBSDKAccessToken.current() != nil {
                    sSelf.loginWithFacebook()
                } else {
                    sSelf.goToLoginVC()
                }
            }
        }
    }
    
    fileprivate func loginWithFacebook() {
        MStyleServer.logInWithFacebookToken(FBSDKAccessToken.current().tokenString, completion: { [weak self] (result) in
            if result.isSuccess {
                self?.goToMainVC()
            } else {
                self?.goToLoginVC()
            }
        })
    }
    
    // MARK: Segue
    fileprivate func goToMainVC() {
        MStyleServer.getPhotoCount { count in
            showFirstPage = count == 0
            self.performSegue(withIdentifier: self.goMainSegueIdentifier, sender: nil)
        }
    }
    
    fileprivate func goToLoginVC() {
        performSegue(withIdentifier: goLoginSegueIdentifier, sender: nil)
    }
}
