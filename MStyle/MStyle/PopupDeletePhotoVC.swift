//
//  PopupDeletePhotoVC.swift
//  MStyle
//
//  Created by luan on 3/10/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import UIKit

protocol PopupDeleteDelegate: class {
    func deletePhoto(at indexPath: IndexPath)
}

class PopupDeletePhotoVC: UIViewController, UIViewControllerTransitioningDelegate, UIGestureRecognizerDelegate {
    
    // MARK: Outlet
    
    @IBOutlet weak var imageView: UIImageView!
    var image: UIImage?
    var indexPath: IndexPath!
    weak var delegate: PopupDeleteDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        transitioningDelegate = self
        modalPresentationStyle = .custom
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(cancel))
        tapGestureRecognizer.cancelsTouchesInView = false
        tapGestureRecognizer.delegate = self
        view.addGestureRecognizer(tapGestureRecognizer)
        
        imageView.image = image
    }
    
    deinit {
        print("Luan De-init \(self)")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: Navigation transition delegate
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return ClosetPopupPresentation(presentedViewController: presented, presenting: presenting)
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return BounceAnimationController()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return SlideOutAnimationController()
    }
    
    
    // MARK: Gesture Recognize Delegate
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view === self.view
    }
    
    // MARK: Action
    @IBAction func cancel() {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func delete() {
        
        dismiss(animated: true, completion: nil)
        delegate?.deletePhoto(at: indexPath)
        
    }
}
