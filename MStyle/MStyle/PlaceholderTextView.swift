//
//  PlaceholderTextView.swift
//  MStyle
//
//  Created by luan on 9/23/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit

@IBDesignable
class PlaceholderTextView: UITextView, UITextViewDelegate {

    @IBInspectable var placeholder: String? {
        didSet {
            if showingPlaceholder {
                text = placeholder
            }
        }
    }
    @IBInspectable var placeholderColor: UIColor? {
        didSet {
            if showingPlaceholder {
                textColor = placeholderColor
            }
        }
    }
    
    fileprivate var originTextColor: UIColor?
    fileprivate var showingPlaceholder = true {
        didSet {
            if showingPlaceholder {
                text = placeholder
                textColor = placeholderColor
            } else {
                text = ""
                textColor = originTextColor
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        originTextColor = textColor
        text = placeholder
        textColor = placeholderColor
        delegate = self
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if showingPlaceholder {
            showingPlaceholder = false
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if text.isEmpty {
            showingPlaceholder = true
        }
    }
}
