//
//  SortTypeCell.swift
//  MStyle
//
//  Created by luan on 9/4/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import UIKit

class SortTypeCell: UITableViewCell {

    @IBOutlet weak var checkmarkImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
