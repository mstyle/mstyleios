//
//  ClosetFilterButton.swift
//  MStyle
//
//  Created by luan on 6/20/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import UIKit

@IBDesignable
class ClosetFilterButton: UIButton {

    @IBInspectable var normalTintColor: UIColor?
    @IBInspectable var selectedTintColor: UIColor?
    
    var isChoice: Bool = false {
        didSet {
            tintColor = isChoice ? selectedTintColor : normalTintColor
        }
    }
}

