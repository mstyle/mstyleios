//
//  UIView.swift
//  MStyle
//
//  Created by Phu Quang Nguyen on 5/18/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func animateConstraintWithDuration(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, options: UIViewAnimationOptions = [], completion: ((Bool) -> Void)? = nil) {
        UIView.animate(withDuration: duration, delay:delay, options:options, animations: { [weak self] in
            self?.setNeedsLayout()
            self?.layoutIfNeeded()
            }, completion: completion)
    }
    
    func show(_ duration: TimeInterval = 0.2) {
        isHidden = false
        UIView.animate(withDuration: duration, animations: { 
            self.alpha = 1
        }) 
    }
    
    func hide(_ duration: TimeInterval = 0.2) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0
            }, completion: { _ in
                self.isHidden = true
        }) 
    }
    
    func moveTo(_ position: CGPoint, fromPosition: CGPoint, duration: TimeInterval = 0.3, options: UIViewAnimationOptions = []) {
        center = fromPosition
        UIView.animate(withDuration: duration, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.center = position
            }, completion: nil)
    }
}
