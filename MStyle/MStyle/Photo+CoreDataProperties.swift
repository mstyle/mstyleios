//
//  Photo+CoreDataProperties.swift
//  MStyle
//
//  Created by luan on 10/23/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Photo {

    @NSManaged var contentType: String
    @NSManaged var height: NSNumber
    @NSManaged var id: NSNumber?
    @NSManaged var localId: NSNumber?
    @NSManaged var separator1: NSNumber
    @NSManaged var separator2: NSNumber
    @NSManaged var separator3: NSNumber?
    @NSManaged var size: NSNumber
    @NSManaged var typeString: String
    @NSManaged var userId: NSNumber?
    @NSManaged var width: NSNumber
    @NSManaged var discussion: String?
    @NSManaged var createdDate: Date
    @NSManaged var modeString: String
    @NSManaged var favorited: Bool
}
