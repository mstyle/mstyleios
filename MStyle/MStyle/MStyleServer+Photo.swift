//
//  MStyleServer+Photo.swift
//  MStyle
//
//  Created by luan on 9/25/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import Foundation
import Alamofire

extension MStyleServer {
    
    // MARK: Get Photo Count
    static func getPhotoCount(_ completion: @escaping (Int) -> Void) {
        let URL = HOST + GET_PHOTO_COUNT_END_POINT
        
        let request = Alamofire.request(URL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
        request.validate().responseJSON { response in
            if case .success(let value) = response.result, let dictionary = value as? [String: AnyObject], let count = dictionary["count"] as? Int
            {
                completion(count)
            } else {
                completion(0)
            }
        }
    }
    
    // MARK: Upload Photo
    
    static func addPhoto(_ photo: Photo, completion: ((DataResponse<Any>?) -> Void)? = nil) {
        
        guard let data = photo.data else {
            completion?(nil)
            return
        }
        
        let parameters = photo.dictionary
        var jsonData: Data? = nil
        do {
            jsonData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch {
            print("Error in JSON conversion: \(error)")
            completion?(nil)
            return
        }
        //let jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding)! as String
        let url = HOST + "api/photos/addNupload?"
        
        let accessToken = KeychainHelper.getStringForKey(KeychainConstaint.kAccessTokenKey) ?? ""
        let headers = [
            "Authorization": "Bearer \(accessToken)",
            "Content-Type": "multipart/form-data"
        ]
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(data, withName: "file", fileName: "photo.jpg", mimeType: "image/jpeg")
            multipartFormData.append(jsonData!, withName: "photo")
        }, usingThreshold: UInt64.init(),
           to: url,
           method: .post,
           headers: headers,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    debugPrint(response)
                    completion?(response)
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        })
    }
    
    static func updatePhotoMode(_ photo: Photo, completion: @escaping (Bool) -> Void) -> DataRequest {
        let parameters = photo.dictionary
        let URL = HOST + PHOTO_MODE
        
        let request = Alamofire.request(URL, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        request.validate().response { response in
            completion(response.error == nil)
        }
        return request
    }
    
    fileprivate static func createPhoto(_ photo: Photo, completion: @escaping (_ photo: Photo) -> Void) {
        let parameters = photo.dictionary
        let URL = HOST + PHOTO_END_POINT
        
        let request = Alamofire.request(URL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        request.validate().responseJSON { response in
            
            debugPrint("============================")
            debugPrint("createPhoto")
            debugPrint("============================")
            debugPrint("\(response)")
            debugPrint("============================")
            
            switch response.result {
            case .failure(let error):
                debugPrint(error)
            case .success(let value):
                if let dictionary = value as? [String: AnyObject] {
                    photo.updateUserInfo(dictionary)
                }
            }
            completion(photo)
        }
    }
    
    static func savePhoto(_ photo: Photo, completion: @escaping (Bool) -> Void) {
        guard let id = photo.id else {
            completion(false)
            return
        }
        
        let URL = HOST + String(format: SAVE_PHOTO, id.int32Value)
        let request = Alamofire.request(URL, method: .post, parameters: nil, encoding: URLEncoding.default, headers: headers)
        request.validate().response { [weak photo] response in
            guard response.error == nil else {
                completion(false)
                return
            }
            guard let photo = photo else {
                completion(false)
                return
            }
            guard let context = CoreDataManager.context else {
                completion(false)
                return
            }
            photo.saveTo(context: context)
            CoreDataManager.save()
            User.currentUser.photos.append(photo)
            completion(true)
        }
    }
    
    // MARK: Get Download Photo
    static func getAllPhotos(_ completion: ((DataResponse<Any>) -> Void)? = nil) {
        let URL = HOST + GET_ALL_PHOTO_END_POINT
        
        let request = Alamofire.request(URL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
        request.validate().responseJSON { response in
            completion?(response)
        }
    }
    
    static var tempID = 0
    static func getTempURL() -> URL {
        tempID += 1
        var filename = "tempPhoto\(tempID).jpg"
        filename = (applicationCachesDirectory as NSString).appendingPathComponent(filename)
        let tempURL = URL(fileURLWithPath: filename)
        let path = tempURL.path
        if FileManager.default.fileExists(atPath: path) {
            do {
                try FileManager.default.removeItem(atPath: path)
            } catch {
                print("Error when remove file at path \(tempURL) with error: \(error)")
            }
        }
        return tempURL
    }
    
    static func downloadPhoto(_ id: Int, size: CGSize, completion: ((UIImage?) -> Void)? = nil) -> Request {
        
        let url = HOST + String(format: DOWNLOAD_PHOTO_END_POINT, id)
        let tempURL = getTempURL()
        let maxSize = max(Int(size.width), Int(size.height))
        let parameters = [
            "width": maxSize,
            "height": maxSize
        ]
        
        let request = MStyleServer.notCacheManager.download(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers) { (url, response) -> (destinationURL: URL, options: DownloadRequest.DownloadOptions) in
            let path = tempURL.path
            if FileManager.default.fileExists(atPath: path) {
                do {
                    try FileManager.default.removeItem(atPath: path)
                } catch {
                    print("Error when remove file at path \(tempURL) with error: \(error)")
                }
            }
            return (tempURL, DownloadRequest.DownloadOptions.createIntermediateDirectories)
        }
        request.response { (response) in
            if let _ = response.error {
                completion?(nil)
            } else if let desURL = response.destinationURL {
                do {
                    let data = try Data(contentsOf: desURL)
                    let image = UIImage(data: data)
                    completion?(image)
                } catch {
                }
            }
        }
        return request
    }
    
    static func downloadPhoto(_ photo: Photo, completion: ((Bool) -> Void)? = nil) {
        guard let id = photo.id else {
            completion?(false)
            return
        }
        guard let photoURL = photo.photoURL else {
            completion?(false)
            return
        }
        
        if photo.downloadRequest == nil {
            let url = HOST + String(format: DOWNLOAD_PHOTO_END_POINT, id.int32Value)
            
            photo.downloadRequest = MStyleServer.notCacheManager.download(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers) { (url, response) -> (destinationURL: URL, options: DownloadRequest.DownloadOptions) in
                let path = photoURL.path
                if FileManager.default.fileExists(atPath: path) {
                    do {
                        try FileManager.default.removeItem(atPath: path)
                    } catch {
                        print("Error when remove file at path \(photoURL) with error: \(error)")
                    }
                }
                return (photoURL, DownloadRequest.DownloadOptions.createIntermediateDirectories)
            }
            photo.downloadRequest?.response { [weak photo] (response) in
                photo?.downloadedPhoto(response.error)
                photo?.downloadRequest = nil
                
                completion?(response.error == nil)
            }
        }
    }
    
    // MARK: Delete Photo
    static func deletePhotos(_ photos: [Photo], completion: @escaping ((Bool) -> Void)) {
        guard photos.count > 0 else {
            completion(false)
            return
        }
        
        var ids = "["
        for (index,photo) in photos.enumerated() {
            guard let id = photo.id else {
                continue
            }
            ids = "\(ids)\(id.intValue)" + (index == photos.count - 1 ? "" : ",")
        }
        ids = "\(ids)]"
        let params = [
            "jsonIds": ids
        ]
        
        let url = HOST + DELETE_PHOTOS
        let request = Alamofire.request(url, method: .delete, parameters: params, encoding: URLEncoding.default, headers: headers)
        request.validate().response { (response) in
            completion(response.error == nil)
        }
    }
    
    static func deletePhoto(_ photo: Photo, completion: @escaping ((Bool) -> Void)) {
        guard let id = photo.id else {
            completion(false)
            return
        }
        
        let request: DataRequest = {
            let isReference = photo.userId != nil && photo.userId!.intValue != User.currentUser.id
            if isReference {
                let URL = HOST + String(format: UNSAVE_PHOTO, id.int32Value)
                return Alamofire.request(URL, method: .post, parameters: nil, encoding: URLEncoding.default, headers: headers)
            } else {
                let URL = HOST + String(format: DELETE_PHOTO, id.int32Value)
                return Alamofire.request(URL, method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: headers)
            }
        }()
        request.validate().response { (response) in
            guard response.error == nil else {
                completion(false)
                print("Delete photo error \(response.error!.localizedDescription)")
                return
            }
            CoreDataManager.delete(id.intValue)
            completion(true)
        }
    }
    
    // MARK: Like
    static func likePhoto(_ photo: Photo, completion: @escaping (Bool) -> Void) {
        guard let id = photo.id else {
            return
        }
        let URL = HOST + String(format: LIKE_PHOTO, id.int32Value)
        
        let request = Alamofire.request(URL, method: .post, parameters: nil, encoding: URLEncoding.default, headers: headers)
        request.validate().response { (response) in
            completion(response.error == nil)
        }
    }
    
    static func unlikePhoto(_ photo: Photo, completion: @escaping (Bool) -> Void) {
        guard let id = photo.id else {
            return
        }
        let URL = HOST + String(format: UNLIKE_PHOTO, id.int32Value)
        let request = Alamofire.request(URL, method: .post, parameters: nil, encoding: URLEncoding.default, headers: headers)
        request.validate().response { (response) in
            completion(response.error == nil)
        }
    }
}
