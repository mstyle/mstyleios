//
//  TagShareLayout.swift
//  MStyle
//
//  Created by luan on 9/27/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit

protocol TagShareLayoutDelegate: class {
    func collectionView(_ collectionView: UICollectionView, sizeTagAtIndexPath indexPath:IndexPath) -> CGSize
    func sizeForAddButton(_ collectionView: UICollectionView) -> CGSize
    func collectionView(_ collectionView: UICollectionView, newHeight height: CGFloat)
}

@IBDesignable
class TagShareLayout: UICollectionViewLayout {
    weak var delegate: TagShareLayoutDelegate!
    
    @IBInspectable var marginHor: CGFloat = 0
    @IBInspectable var marginVer: CGFloat = 0
    @IBInspectable var itemSpacing: CGFloat = 0
    @IBInspectable var rowSpacing: CGFloat = 0
    
    fileprivate var itemSize = CGSize()
    fileprivate var cacheAttributes = [UICollectionViewLayoutAttributes]()
    
    fileprivate var width: CGFloat {
        get {
            return collectionView!.bounds.width
        }
    }
    fileprivate var contentHeight: CGFloat = 0
    var ratio: CGFloat = 1.0
    
    override var collectionViewContentSize : CGSize {
        return CGSize(width: width, height: contentHeight)
    }
    
    override func prepare() {
        let count = collectionView!.numberOfItems(inSection: 0)
        if cacheAttributes.count != count {
            cacheAttributes = [UICollectionViewLayoutAttributes]()
            
            
            var xOffset = marginHor
            var yOffset = marginVer
            let addButtonSize = delegate.sizeForAddButton(collectionView!)
            let rightMargin = width - marginHor - itemSpacing
            let rightMarginWithButton = rightMargin - addButtonSize.width
            
            for item in 0..<count {
                let indexPath = IndexPath(item: item, section: 0)
                let size = delegate.collectionView(collectionView!, sizeTagAtIndexPath: indexPath)
                let maxXOffset = xOffset + size.width
                
                if maxXOffset > rightMargin ||
                    (maxXOffset > rightMarginWithButton && item == count - 1) {
                    xOffset = marginHor
                    yOffset += size.height + rowSpacing
                }
                
                let frame = CGRect(x: xOffset, y: yOffset, width: size.width, height: size.height)
                
                let attribute = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attribute.frame = frame
                
                cacheAttributes.append(attribute)
                
                xOffset += itemSpacing + size.width
                contentHeight = yOffset + size.height + marginVer
            }
            delegate.collectionView(collectionView!, newHeight: contentHeight)
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes = [UICollectionViewLayoutAttributes]()
        for attributes in cacheAttributes {
            if attributes.frame.intersects(rect) {
                layoutAttributes.append(attributes)
            }
        }
        
        return layoutAttributes
    }
}
