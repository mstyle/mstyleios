//
//  SortViewController.swift
//  MStyle
//
//  Created by luan on 9/4/17.
//  Copyright © 2017 Phu Quang Nguyen. All rights reserved.
//

import UIKit

class SortViewController: UIViewController {

    var sortType: SortType = .date
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        transitioningDelegate = self
        modalPresentationStyle = .custom
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func back() {
        dismiss(animated: true, completion: nil)
    }
}

extension SortViewController: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return ClosetPopupPresentation(presentedViewController: presented, presenting: presenting)
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return BounceAnimationController()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return SlideOutAnimationController()
    }
}

extension SortViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SortType.values.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SortTypeCell", for: indexPath) as! SortTypeCell
        cell.nameLabel.text = SortType.values[indexPath.row].rawValue
        cell.checkmarkImageView.isHidden = SortType.values[indexPath.row] != sortType
        return cell
    }
}

extension SortViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        sortType = SortType.values[indexPath.row]
        tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
    }
}
