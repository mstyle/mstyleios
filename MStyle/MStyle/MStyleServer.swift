//
//  Authorization.swift
//  MStyle
//
//  Created by luan on 8/7/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import Foundation
import Alamofire

struct MStyleServer {
    
    struct AuthorizationKey {
        static let providerId = "providerId"
        static let fbAccessToken = "accessToken"
        static let grant_type = "grant_type"
        static let client_id = "client_id"
        static let client_secret = "client_secret"
        
        static let access_token = "access_token"
        static let refresh_token = "refresh_token"
    }
    
    static let loginHeaders = [
        "Authorization": "Basic TVN0eWxlaU9TYXBwOk1TdHlsZTEwNVNlY3JldA==",
        "Content-Type": "Application/json"
    ]
    
    static var headers: [String: String] {
        let accessToken = KeychainHelper.getStringForKey(KeychainConstaint.kAccessTokenKey) ?? ""
        let headers = [
            "Authorization": "Bearer \(accessToken)",
            "Content-Type": "Application/json"
        ]
       return headers
    }
    
    
    static let HOST = "http://52.69.165.221:8080/"
    
    static let LOG_IN_END_POINT = "oauth/token"
    static let IS_AUTHENTICATED_END_POINT = "api/authenticate"
    
    static let CURRENT_ACCOUNT = "api/users/me"
    
    static let PHOTO_END_POINT = "api/photos"
    static let GET_ALL_PHOTO_END_POINT = "api/photos/closet"
    static let DOWNLOAD_PHOTO_END_POINT = "api/photos/%d/download"
    static let GET_PHOTO_COUNT_END_POINT = "api/photos/count"
    static let DELETE_PHOTO = "api/photos/%d"
    static let DELETE_PHOTOS = "api/photos/batch"
    static let LIKE_PHOTO = "api/photos/like/%d"
    static let UNLIKE_PHOTO = "api/photos/unlike/%d"
    static let SAVE_PHOTO = "api/friends/add/photo/%d"
    static let UNSAVE_PHOTO = "api/friends/remove/photo/%d"
    static let PHOTO_MODE = "api/photos/mode"
    
    static let POST_END_POINT = "api/posts"
    static let WALL_END_POINT = "api/posts/wall"
    static let POST_LIKE_POINT = "api/post-likes"
    
    static let CLIENT_ID = "MStyleiOSapp"
    static let CLIENT_SECRET = "MStyle105Secret"
    
    static let FRIENDS = "api/friends"
    static let FRIENDS_SUGGESTING = "api/friends/suggesting"
    static let FRIENDS_REQUEST = "api/friends/pending"
    static let FRIEND_REQUEST_ACTION = "api/friends/action/request/%d"
    static let FRIEND_ACCEPT_ACTION = "api/friends/action/accept/%d"
    static let FRIEND_DENY_ACTION = "api/friends/action/deny/%d"
    static let FRIEND_PHOTOS = "api/photos/closet/friend/%d"
    
    static let notCacheManager: SessionManager = {
        let config = URLSessionConfiguration.default
        config.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        config.urlCache = nil
        config.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        
        return Alamofire.SessionManager(configuration: config)
    }()
}
